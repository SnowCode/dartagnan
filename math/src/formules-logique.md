## Les formules

$$
 \neg ((P \vee Q) \wedge R) $$


La formule précédente est une combinaison de connecteurs. Les parenthèses sont très importante car comme en arithmétique, les parenthèses indiquent les priorités des opérations, donc $ P \vee Q $ doit être fait avant la plus grande parenthèse, pour enfin terminer par la négation complète.

### Antilogies & Tautologie
$$
 \vdash P \vee \neg P $$


La formule ci-dessus signifie que $ P \vee \neg P $ donnera **toujours** 1. Ca s'appelle une tautologie. Une tautologie signifie qu'une formule sera toujours vraie.

$$
 \vdash \neg (P \wedge \neg P) $$


Tandis qu'une antilogie signifie qu'une formule sera toujours fausse. 

