# Les connecteurs logiques de base

![Minecraft logic gates](https://www.wonderlandblog.com/.a/6a00d834515f7269e20133f4e0b83b970b-500wi)

| Sym | Nom mathématique | Electronique | Java |Minecraft |
| ---- | ---- | ---- | ---- | ---- |
| $ \neg $ | Negation | `NOT` | `!` | Une torche de redstone sur un bloc avec un levier sur le bloc (inverseur) |
| $ \wedge $ | Conjonction | `AND` | `&&` | 2 torches activée par des leviers, reliées par une poudre de redstone et un inverseur |
| $ \vee $ | Disjonction | `OR` | `\|\|` (ou inclusif) | Une poudre de redstone qui relie 2 leviers |
| $ \oplus $ | Disjonction exclusive | `XOR` | `^` (ou exclusif) | Trop compliqué à décrire |
| $ \iff $ | Equivalence | `XNOR` | `==` | XOR avec un inverseur |

## La négation $ \neg $

La négation correspond à "Il est faut que P" (P étant une proposition)

Donc P prends la valeur contraire de $ \neg P $.

| P | $ \neg P $ | $ \neg \neg P $ |
| --- | --- | --- |
| 0 | 1 | 0 |
| 1 | 0 | 1 |

Ceci est une table de vérité qui représente différentes valeurs d'énoncés par rapport à leur proposition(s).

## La conjonction $ \wedge $
La conjonction est vraie si 2 propositions sont vraies. 

| P | Q | $ P \wedge Q $ |
| --- | --- | --- |
| 0 | 0 | 0 |
| 1 | 0 | 0 |
| 0 | 1 | 0 |
| 1 | 1 | 1 |

## La disjonction $ \vee \))
La disjonction est vraie si une des 2 propositions sont vraie.

| P | Q | $ P \vee Q $ |
| --- | --- | --- |
| 0 | 0 | 0 |
| 1 | 0 | 1 |
| 0 | 1 | 1 |
| 1 | 1 | 1 |

## Implication $ \implies $
$$
 P \wedge Q \implies P \implies Q $$


Cet énoncé signifie que si P et Q sont vrai (AND) alors P est vrai et Q est vrai. L'implication correspond à "alors". Donc si XYZ alors ABC.

$$
 \neg(x < y) \implies x = y $$


On peut par exemple imaginer une situation comme celle ci dessus, si x n'est pas plus petit que y alors x est égal à y. 

| P | Q | $ P \wedge Q $ | $ P \implies Q $ | $Q \implies P $ |
| --- | --- | --- | --- | --- |
| 0 | 0 | 0 | 1 | 1 |
| 0 | 1 | 0 | 1 | 0 |
| 1 | 0 | 0 | 0 | 1 |
| 1 | 1 | 1 | 1 | 1 |

Contrairement aux conjonctions (AND), si la première proposition (l'antécédent) est faux, alors l'implication est forcément vraie car le contraire n'a pas été prouvé. 

Donc les deux propositions ne sont pas interchangables comme vu dans le tableau les colonnes 4 et 5 ne sont pas les mêmes. 
BB
Un peu de vocabulaire :

| Antécédent | Conséquent | Enoncé | Réciproque | Contraposée |
| --- | --- | --- | --- | --- | 
| P | Q | $ P \implies Q $ | $Q \implies P $ | $\neg Q \implies \neg P $ |

### Equivalence $ \iff $
| P | Q | $P \iff Q $ | $Q \iff P $ |
| --- | --- | --- | --- |
| 0 | 0 | 1 | 1 |
| 1 | 0 | 0 | 0 |
| 0 | 1 | 0 | 0 |
| 1 | 1 | 1 | 1 |

L'équivalence est une genre d'égalité dans la logique. Cela signifie que P corresponds à Q. Les deux propositions sont interchangables.

### Disjonction exclusive $ \oplus $
$$
 P \oplus Q \iff (P \wedge \neg Q) \vee (\neg P \wedge Q) \iff \neg (P \iff Q) $$


La disjonction exclusive peut etre représentée par des AND, OR et NOT uniquement, c'est un genre de raccourcis. La disjonction exclusive (XOR) peut aussi être utilisée pour représenter une équivalence en ajoutant une négation (NOT).

| P | Q | $ P \oplus Q $ | $ P \vee Q $ | $P \wedge Q $ |
| --- | --- | --- | --- | --- |
| 0 | 0 | 0 | 0 | 0 |
| 1 | 0 | 1 | 1 | 0 |
| 0 | 1 | 1 | 1 | 0 |
| 1 | 1 | 0 | 1 | 1 |

Une disjonction exclusive (XOR) est comme une disjonction inclusive (OR) sauf que les deux propositions ne peuvent pas être vrai pour que le XOR soit vrai. Donc un XOR sera forcément faux là ou un AND sera vrai.
