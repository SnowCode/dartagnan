### Le modulo
> Le **modulo** est le reste d'une division euclidienne. Dans l'exemple ci dessus : le modulo de 13 par 5 est 3 (le reste de la division). Dans java ou python l'opération modulo est noté par un `%`. Donc `13 % 5` donne `3`.

En revanche d'un language de programmation à un autre, le modulo peut donner une valeur différente quand les valeurs données sont négatives. Python et Excel arrondissent (*floor*) le quotient tandis que Java laisse juste tomber les décimales.

On peut aussi redéfinir un diviseur et un multiple avec le modulo : si $ a \neq 0 $ et $ b mod a = 0 $ alors $a$ est un diviseur de b et b est un multiple de a. 

