## Introduction
Une base de donnée est un système qui permet de mémoriser de manière durable des données surun support physique (mémoire secondaire)

Les données devront pouvoir être crée, lues, modifiées, et supprimées (CRUD, create read update delete).

Les objets réprésentés dans la base de donnée pourront être oganisés de manière à faire apparaitre les relations entre elles.

Tous les informaticiens sont confrontés un jour au l'autre à une base de donnée. Mais il existe aussi un profil specifique dédié à l'administratoin de bases de données (DBA) qui est responsable du bon fonctionnement des serveurs de bases de données et a leur création.

### Qu'est ce qu'un SGBD
Un SGBD est un *Système de Gestion de Bases de Données* dans le but d'optimiser les performances, de protéger l'intégrité des données, même en cas de panne, de seulement autoriser l'accès aux personnes autorisées et de partager des données.

Un SGBD propose aussi des outils divers pour aider à la conception et à la gestion des bases de données.

### Modèle de SGBD
Une base de donnée relationelle est organisée en tables. Chaque table contenant des colones et des lignes. 

Le modèle hierarchique classe les données hierarchiquement selon une arborescence descendante (premier modèle)

Le modèle réseau, une extension du modèle hiérarchique selon une structure de graphe

Le modèle orienté-objet, 

Le modèle spacial

Le modèle XML

NOSQL, qui permet de palier aux limites du modèle relationnel, les données peuvent être mises horizontalement à travers plusieurs instances pour permettre à un serveur à ne pas avoir à gérer tout.

## Architecture ANSI/SPARC et modélisation
### Architecture ANSI/SPARC
L'architecture ANSI/SPARC est organisée en 3 niveaux:

* Le niveau interne (physique) qui définit la structure du stockage sur le support (fichiers, secondaire). Ce niveau interesse principalement les développeurs de SGBD et les DBA. 
* Le niveau conceptuel (décrit l'organisation de l'ensemble des données, et leur contraites). Ce niveau interesse principelement les développeurs.
* Le niveau externe propose aux groupes utilisateurs sa propre vue des données dont il a besoin. 

### Description des données et création de modèles 
Pour créer un schéma de base de données il faut :

* Déterminer comment représetner les objets (caractéristiques, comment l'identifier, etc)
* Déterminer les liens qui unissent les objets (ainsi que les contraites sur ces liens)
* Déterminer les contraites sur les caractéristiques (types, maximum, minimum, longueur, etc)

Généralement ça commence par la création d'un "schéma entité-association" (modèle conceptuel des données) ensuite converti dans un modèle logiqe, puis complété par des éléments techniques tel que les types et les contraites. Enfin les éléments du schéma sont traduits en instructions de création de la bdd.

* Le niveau conceptuel ignore les aspects propre à l'informatique. Se concentre sur le métier de l'organisme et débouche sur un *MCD* (entité-association)
* Le niveau logique (dépends du type de SGBD utilisé) permet d'ajouter des contraites sur les données (types ou autres) et débouche sur un *MLD* (Modèle Logique des Données)
* Le niveau physique se concentre sur les spécificités du SGBD utilisé, ajout des types spécifiques, et création du script SQL pour la création des tables.

## Modèle relationnel
### Domaine (théorique)
Le domaine est l'ensemble des valeurs possible dans une base de donnée. Par exemple: 

* Ensemble des entiers : ${1, 2, 3, 4, 5, ...}$
* Ensemble des entiers allant de 1 à 4 : ${1, 2, 3, 4}$
* Ensemble des noms : ${Robert, Jean, Thomas, Marc, Jules, Simone}$
* ...

En pratique, ce domaine est défini par les types et les contraintes.

### Produit cartésien (théorique)
Le produit cartésien est la "mise en relation" de plusieurs domaines. Par exemple :

Produit cartésien des noms et des entiers allant de 1 à 4 donne 24 combinaisons possibles. Mais seul 6 seront utilisée (une par nom).

### Une relation
Une relation est un sous-ensemble nommé d'un *produit cartésien*. Dans l'exemple précédent, cela donnera par exemple : (toutes les occurences sont des Personnes qui sont donc dans la table "Personnes")

| Nom | Age |
| --- | --- |
| Robert | 2 |
| Jean | 1 |
| Thomas | 2 |
| Marc | 4 |
| Jules | 3 |
| Simone | 1 |

Une relation est représentée par une table. Une table (ou relation) est consituée de :

* Une colonne représente un attribut, une propriété. Par exemple: "Age"
* Une ligne (tuple) représente une occurence et est unique. Par exemple "Simone - 1" est un tuple

La cardinalité d'une relation est le nombre de tuples qui la compose, et le degré de la relation est le nombre d'attributs.

### Schéma d'une relation
Le schéma d'une relation (ou d'une table) consiste de :

* Du nom de la relation, exemple, « *Personnes* »
* De la liste des attributs. Pour chaque attribut est définis:
    * Un nom, exemple, « *Nom* »
    * Un type, exemple, une chaine de caractère de 25 caractères maximum « *varchar(25)* »
    * Potentiellement des contraintes, par exemple, ne peut pas être nulle « *NOT NULL* »

En SQL voici un exemple de schéma:

```sql
CREATE TABLE Personnes ( 
    id INT NOT NULL,
    nom VARCHAR(25) NOT NULL,
    age INT NOT NULL,
    PRIMARY KEY (id)
);
```

Pour juger de la qualité d'un schéma il faut s'assurer que le schéma :

* Ne va pas créer de redondances
* Ne va pas créer d'anomalies
    * Anomalie de mise à jour, une mise à jour d'une information n'est pas répercutée sur toutes le occurences de l'information
    * Anomalie de suppression, la suppression d'un tuple entraine une perte d'information pertinente

<!-- AJOUTER IMAGE ICI -->

### Types de clés
* Les clés **candidates** est un sous-ensemble d'attributs tel que 
    * Doivent être uniques (contraite d'unicité), 2 tuples ne peuvent pas avoir la même valeur pour cet ensemble d'attributs
    * Les attributs présent doivent être absolument nécessaire (contraite d'irreductibilité)

Exemples de clé candidates : Matricule, nom + prénom + addresse, etc.

* La clé **primaire** est un attribut choisi parmis les clés candidates choisi pour identifier un tuple. Dans la représentation une clé primaire sera soulignée.

Exemple de clé primaire : Matricule

* Les clés **étrangères** sont des références à des clés candidates (souvent la clé primaire), d'une autre table ou de la même table, soit de faire des liens entre des tuples.
    * Elle doivent faire référence à une clé primaire qui existe déjà
    * Si une clé étrangère existe pour une clé primaire on ne peut supprimer la clé primaire sans supprimer la clé étrangère. (ces deux règles sontla contraite d'intégrité référentielle)

Exemple de clé étrangère : Mention d'un matricule dans une autre table

<!-- Ajouter image clé étrangère ici -->

* Les **surclés** sont des ensembles qui ne respectent pas la contraite d'irreductibilité des clés candidates. Soit une clé candidates (+ d'autres attributs) = surclés. 

Exemples de surclé : matricule, matricule + nom

### Les contraintes
* La contrainte d'unicité (`UNIQUE`) signifie que qu'il ne peut pas y avoir 2 fois la même valeur pour cet attribut dans la table
* La contrainte `NOT NULL` signifie qu'il faut y avoir une valeur pour cet attribut, il signifie que l'attribut est obligatoire

### Types primitifs des attributs
> Les types peuvent être différents d'un SGBD à un autre. Par exemple dans Oracle, le type `boolean` n'existe pas il faut donc créer une colone `integer` qui a un domaine de $[0,1]$.

* Type `integer`, seulement les nombres entiers
* Type `numeric(precision[, scale])` admet des nombres réels, `precision` indique le nombre de chiffre et `scale` indique le nombre de chiffres décimaux (après la virgule)
* Type `char(longeur)` indique une chaine de caractère qui prends un espace fixe dans la base de donnée.
* Type `varchar(longueur)` indique la même chose que le précédent, sauf que l'espace est variable
* Type `date` indique une date (jour, mois, année)
* Type `timestamp` indique une date et une heure
* Type `boolean` admet les valeurs "vrai" ou "faux" (1 ou 0)

### Les opérateurs relationnels (algèbre relationnelle)
<!-- Ajouter exemples -->

* La sélection (`SELECT ... WHERE ...`) qui permet d'obtenir une nouvelle relation de même schéma qui satisfairont une condition donnée. 
* La projection (`SELECT ...`) permet d'obtenir, par une sélection, seulement certains attributs.
* L'union de deux relations permet de combiner deux tables (ayant le même schéma) en une seule liste de tuple.
* L'intersection est comme l'union, elle combine deux table, sauf que contrairement à l'union, elle supprimme les doublons de tuples.
* La jointure
* 

## SQL
SQL veut dire Structured Query Language et permet d'intéragir avec les bases de données. Les SGBD étant différents, les languages SQL varie aussi même si certaines choses sont standardisées. 

SQL est divisé en 4 parties

* DDL (Data Definition Language) qui permet de gérer le schéma d'une base de donnée 
* DML (Data Manipulation Language) qui permet de gérer les données en elle même (les ajouter, supprimer, modifier et lire)
* DCL (Data Control Language) qui permet de grérer les permissions au données pour chaque utilisateur SGBD. Ces utilisateurs ne correspondent pas aux utilisateurs finaux mais bien aux utilisateurs du SGBD (donc seulement sur le serveur)
