# Projet Swilinux
Le *véritable* OS divin.

## Config v1 idée
* Arch Linux
* bspwm
* sxhkd
* polybar
* alacritty
* feh & pywal
* picom 
* Iosevka font
* Pipewire pour le son

## Todo
* Trouver les différents composants de la config
* Trouver un fond d'écran
* Ajouter plein d'alias
* Installer et tester
* Ecrire la documentation
