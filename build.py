import os, markdown

def build_file(filename, template):
    if filename.endswith(".md"):
        markdown_content = open(filename).read()
        html_content = markdown.markdown(markdown_content, extensions=['tables', 'fenced_code'])
        final_page = template.replace("${html}", html_content)
        final_name = filename.replace(".md", ".html")
        open(final_name, 'w').write(final_page)
        print(f"{filename} built successfully")

template = open("template.html").read()
filename = input("Filename> ")

if filename == "":
    for f in os.listdir():
        build_file(f, template)
else:
    build_file(filename, template)


