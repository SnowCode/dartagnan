# Phase préparatoire de SimulationEmprunt
## Résultats attendus
* Taux du taux mensuel en pourcentage
* Nombre de paiements
* Remboursement mensuel
* Interet pret et total rembouré pour deux périodes

## Données connues
* La formule du taux et remboursement mensuel à partir du taux annuel

## Données de départ
* Capital en €
* Taux annuel en % 
* Durée 1 en années
* Durée 2 en années

## Choisir un premier cas concret
* Capital : 25000€
* Taux annuel % : 3.04 %
* Durée 1 : 15 ans
* Durée 2 : 20 ans

## Solutionner le premier cas concret
* Conversion du taux annuel : $t_a = 3.04 / 100 = 0.0304$
* Calcul du taux mensuel : $t_m = (1 + 0.0304)\^{\frac{1}{12}} - 1 = 0.0024987$

Pour la durée 1 :

* Calcul du nombre de paiements : $N_1 = 15 * 12 = 180$
* Calcul du remboursement mensuel : $M_1 = 25000 \frac{0.0024987}{1-(1+0.0024987)\^{-180}} = 172.63$
* Calcul du remboursement total : $T_1 = 172.63 * 180 = 31072.81$
* Calcul de l'interet pret : $I_1 = 31072.81 - 25000 = 6072.81$

Pour la durée 2 :

* Calcul du nombre de paiements : $N_2 = 20 * 12 = 240$
* Calcul du remboursement mensuel : $M_2 = 25000 \frac{0.0024987}{1-(1+0.0024987)\^{-240}} = 138.63$
* Calcul du remboursement total : $T_2 = 138.63 * 240 = 33271.2$
* Calcul de l'interet pret : $I_2 = 33271.2 - 25000 = 8271.2$

## Solutionner d'autres cas concrets
N/A

## Répertorier les étapes textuellement
* Conversion du taux annuel
* Calcul du taux mensuel
* Pour chaque durée :
    * Calcul du nombre de paiements
    * Calcul du remboursement mensuel
    * Calcul du remboursement total
    * Calcul de l'interet pret

