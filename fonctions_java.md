# Les fonctions vues pour Java
## Affichage Console
####  System.out.print()
#### System.out.println()
#### Systeme.out.printf()

## Input
#### Console.lireInt()
#### Console.lireDouble()
#### Console.lireString()

## Gestion de String
#### myString.charAt(int)
- Retourne le caractère à la position donnée en argument.
#### myString.length()
- Retourne la longueur du string donné en instance.
#### myString.substring(int, int)
- Retourne le string dont les positions ont été donné en argument

#### myString.indexOf('char')
- Renvoi la position du caractère spécifié en argument dans le string donné en instance.
#### myString.startWith("String")
- Renvoi boolean(true) si le string spécifié en argument débute celui donné en instance.
#### myString.endWith("String")
- Renvoi boolean(true) si le string spécifié en argument termine celui donné en instance.

#### myString.toUpperCase()
- transforme le string pour que tous les caractères soient des majuscules.
#### myString.toLowerCase()
- transforme le string pour que tous les caractères soient des minuscules.

## Transformer type de variable
#### Integer.parseInt("myString")
- Retourne un string en int
#### Double.parseDouble("myString")
- Retourne un string en Double
#### String.valueOf(int) //ou Double
- Retourne un int ou Double en String

## Les Maths 
#### Math.random()
- Retourne un double entre [0;1[
#### Math.pow(-base-, -exposant-)
#### Math.sqrt(-base-, -racine-)

### Les arondis
#### Math.ceil()
- Prend la valeur et l'arrondi vers le haut
#### Math.floor()
- Prend la valeur et l'arrondi vers le bas
#### Math.round()
- Prend la valeur et l'arrondi (renvoi un int)
#### Math.rint()
- Prend la valeur et l'arrondi

