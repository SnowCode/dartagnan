# Comment faire un vpn sur un Raspberry Pi
> Si tu veux faire ce tutoriel sur autre chose qu'un rpi, vérifie que le kernel (`uname -r`) est une version au délà de 4.19

Ce tutoriel n'a pas la manière la plus "propre" d'ajouter un client. Normalement une paire de clé et une configuration doit être générée par l'utilisateur et puis la clé publique doit être envoyée au serveur pour complèter la config serveur. Mais c'est un peu compliqué et chiant, alors j'ai généré toute la configuration sur le serveur et envoyé au client.

Aussi, pour ceux qui aurait un kernel plus ancien et qui obtiendrait une erreur au moment de lancer *wireguard*, j'ai mis une section *troubleshooting* à la fin de ce tutoriel.

Dans ce tutoriel:

* Configurer un raspberry PI sans écran, clavier ou souris à connecter dessus (headless)
* Ouvrir les ports d'un modem
* Installer et configurer Wireguard pour un serveur Linux
* Ajouter et configurer des clients
* Se connecter à un serveur Wireguard depuis un téléphone
* Faire fonctionner Wireguard sur des kernels plus anciens

<video controls src='wireguard.mp4'></video>

## Matériel requis
* Un ordinateur
* Un raspberry Pi (dans ce cas, un Rpi4)
* Une carte micro SD (et un adaptateur pour l'ordi si besoin)
* Une alimentation pour le raspberry
* Un cable éthernet pour une connection optimale

## Préparation du raspberry
* D'abord il faut installer l'OS de raspberry, dans ce cas ci, on va utiliser [rpi-imager](https://raspberrypi.com/software) pour l'installer sur la micro SD et on va choisir *Raspberry Pi OS Lite*. 

```bash
yay rpi-imager
sudo rpi-imager
```

* Dans l'installateur, sélectionne `Raspberry Pi OS Lite` pour l'OS, puis ta carte micro SD, puis va dans les paramètres et configure le.
* Brancher le Raspberry Pi et attendre un peu, ensuite se connecter via SSH: (avec le mot de passe choisi dans l'étape précédente)

```bash
ssh pi@raspberrypi
```

## Préparation du réseau
* Trouve l'addresse privée de ton raspberry pi

```bash
hostname -I
```

* Va sur `http://192.168.1.1` et connecte toi (les identifiants sont souvent écrit derrière le modem)
* Va dans la redirection des ports et ajoute la règle suivante:

| Protocol | Début du port externe | Fin du port externe | Port interne | Hôte interne | Nom |
| --- | --- | --- | --- | --- |
| `UDP` | `53` | `53` | `53` | `<ip locale du raspberry pi>` | `wireguard` |

## Installation de Wireguard (client et serveur)
* Installer le package `wireguard` et `qrencode` (pour créer des QR codes pour partager la config avec un téléphone)

```bash
sudo su
apt install wireguard qrencode
```

* Aller dans le dossier `/etc/wireguard`, créer une paire de clés et créer un fichier de configuration `wg0.conf`

```bash
cd /etc/wireguard
wg genkey > private
wg pubkey < private > public
nano wg0.conf
```

## Configuration du serveur
* Coller la configuration suivante. L'addresse est une addresse LAN qui sera utilisée pour représenter le serveur. Les valeurs ne sont pas choisie au hazard elles ont été choisie pour être sur qu'elle ne soit pas déjà utilisée sur le réseau ailleurs. Et le port `53` est utilisé car c'est un port `UDP` qui n'est en général pas bloqué par les firewalls.

```toml
[Interface]
Address = 192.168.215.1/24
PrivateKey = PRIVATE_SERVER_KEY_HERE
ListenPort = 53
PostUp   = sysctl -w net.ipv4.ip_forward=1; sysctl -w net.ipv6.conf.all.forwarding=1; iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o NETWORK_INTERFACE_HERE -j MASQUERADE; ip6tables -A FORWARD -i wg0 -j ACCEPT; ip6tables -t nat -A POSTROUTING -o NETWORK_INTERFACE_HERE -j MASQUERADE
PostDown = sysctl -w net.ipv4.ip_forward=0; sysctl -w net.ipv6.conf.all.forwarding=0; iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o NETWORK_INTERFACE_HERE -j MASQUERADE; ip6tables -D FORWARD -i wg0 -j ACCEPT; ip6tables -t nat -D POSTROUTING -o NETWORK_INTERFACE_HERE -j MASQUERADE
```

* Remplacer `PRIVATE_SERVER_KEY_HERE` par la clé privée générée plus tôt

```bash
sed -i "s|PRIVATE_SERVER_KEY_HERE|$(cat private)|g" wg0.conf
```

* Remplacer `NETWORK_INTERFACE_HERE` par ton interface réseau (souvent `wlan0` pour le wifi et `eth0` pour ethernet). Lance la commande `ip link` pour le savoir.

```bash
ip link
sed -i "s|NETWORK_INTERFACE_HERE|entre ton interface ici|g" wg0.conf
```

## Ajouter un client (peer)
* Installer wireguard sur le client (iOS, Android, Windows, macOS, Linux): [voir ici](https://www.wireguard.com/install/)
* Générer les clés et réouvrir la configuration

```bash
wg genkey > private_client
wg pubkey < private_client > public_client
nano wg0.conf
```

* Ajouter le bloc suivant (changer l'IP pour chaque client pour quelle soit unique)

```toml
[Peer]
PublicKey = PUBLIC_CLIENT_KEY_HERE
AllowedIPs = 192.168.215.2/32
```

* Créer une configuration client

```bash
nano client.conf
```

* Coller ce template (pareil pour l'ip ici)

```toml
[Interface]
Address = 192.168.215.2/32
PrivateKey = PRIVATE_CLIENT_KEY_HERE

[Peer]
PublicKey = PUBLIC_SERVER_KEY_HERE
Endpoint = PUBLIC_SERVER_IP_HERE:53
AllowedIPs = 0.0.0.0/0, ::/0
PersistentKeepalive = 25
```

* Ajouter les clés dans les fichiers de configuration

```bash
sed -i "s|PRIVATE_CLIENT_KEY_HERE|$(cat private_client)|g" *.conf
sed -i "s|PUBLIC_CLIENT_KEY_HERE|$(cat public_client)|g" *.conf
sed -i "s|PUBLIC_SERVER_KEY_HERE|$(cat public)|g" *.conf
```

* Ajouter l'IP publique du serveur (il est possible que cette dernière change et que tu doives modifier les ré-ajouter toi-même) 

```bash
sed -i "s|PUBLIC_SERVER_IP_HERE|$(curl ifconfig.me)|g" *.conf
```

* Partager la configuration que le client devra ajouter: 

```bash
# Pour un ordi (sur linux, ajouter cette configuration dans /etc/wireguard/wg0.conf; sur windows ou mac, ajouter cette configuration dans l'app)
cat client.conf

# Pour un téléphone
cat client.conf | qrencode -t UTF8
```

* Redémarrer le VPN pour appliquer le changement de configuration (ceci est à faire autant sur le serveur que sur le client)

```bash
systemctl restart wg-quick@wg0
```

* Une fois ajouté, supprimer les fichiers de configuration client du serveur

```bash
rm private_client public_client client.conf
```

## Lancer ou éteindre le VPN (client et serveur)
* Relancer le VPN (après un changement de configuration)

```bash
systemctl restart wg-quick@wg0
```

* Lancer le VPN au démarrage du PI

```bash
systemctl enable wg-quick@wg0
```

* Démarrer le VPN maintenant

```bash
systemctl start wg-quick@wg0
```

* Éteindre le VPN maintenant

```bash
systemctl stop wg-quick@wg0
```

* Désactiver le démarrage automatique

```bash
systemctl disable wg-quick@wg0
```

## Troubleshooting
### Kernel trop ancien (Operation not permitted)
* Trouver la version du kernel

```bash
uname -r
```

* Installer les `linux-headers` qui correspondent (en fonction du résultat de la commande précédente, installe le paquet qui correspond) **Installer seulement un seul paquet**

```bash
sudo apt install linux-headers-cloud-amd64
sudo apt install linux-headers-4.19.0-20-amd64
# ou autre encore
```

* Installer `wireguard-dkms` et redémarrer

```bash
sudo apt install --reinstall wireguard-dkms
reboot
```

* Ajouter le kernel module de wireguard et redémarrer à nouveau

```bash
sudo modprobe wireguard
reboot
```

* Tout devrait fonctionner à présent
