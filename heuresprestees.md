# Phase préparatoire de HeuresPrestees
## Résultats attendus
* Durée hebdomadaire
* Montant total à facturer

## Données connues
* Le taux horaire ($18.75$ €)
* Les jours de la semaine

## Données de départ
* Le nombres d'heures et de minutes de prestations de chaque jour du lundi au vendredi en format h:mm

## Choisir un premier cas concret
* Prestations du lundi : 7:30
* Prestations du mardi : 6:45
* Prestations du mercredi : 4:20
* Prestations du jeudi : 7:00
* Prestations du vendredi : 5:55

## Solutionner le premier cas concret
* Calculer le taux minute à partir du taux horaire : $T_m = 18.75 / 60 = 0.3125$
* Répèter pour chaque jour :
    * Séparer les heures et les minutes : $7$ heures et $30$ minutes
    * Convertir les heures en minutes : $m = 7 * 60 = 420$
    * Additioner le total des minutes (additioner avec le nombre de minutes précédentes aussi) : $m = 420 + 30 = 450$
    * Calculer le montant à partir du total des minutes : $450 * 0.3125 = 140.625$
* Calculer le nombre total d'heures et minutes : $1890 = 31 * 60 + 30$ soit $31$ heures et $30$ minutes

## Solutionner d'autres cas concrets
* Calculer le taux minute à partir du taux horaire : $T_m = 18.75 / 60 = 0.3125$
* Répèter pour chaque jour :
    * Séparer les heures et les minutes : $11$ heures et $30$ minutes
    * Convertir les heures en minutes : $m = 11 * 60 = 660$
    * Additioner le total des minutes (additioner avec le nombre de minutes précédentes aussi) : $m = 660 + 30 = 690$
    * Calculer le montant à partir du total des minutes : $690 * 0.3125 = 215.625$
* Calculer le nombre total d'heures et minutes : $2130 = 35 * 60 + 30$ soit $35$ heures et $30$ minutes 

## Répertorier les étapes textuellement
* Calculer le taux minute à partir du taux horaire
* Répèter pour chaque jour :
    * Séparer les heures et les minutes
    * Convertir les heures en minutes
    * Additioner le total des minutes (additioner avec le nombre de minutes précédentes aussi)
    * Calculer le montant à partir du total des minutes
* Calculer le nombre total d'heures et minutes

