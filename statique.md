# Créer un site statique très simplement
Voici un petit tuto pour créer un site statique en quelque lignes de shell sur Linux.

## Dépendences
* `markdown` (commande) provenant du paquet `discount`
* `sshfs` (pour publier les changements directements par SFTP sur un serveur)

## La base du site
* Créer un dossier et monter un dossier distant dedans (par SFTP)

```bash
mkdir mysite/
sshfs mysite/ <utilisateur>@<hôte>:<chemin vers le dossier cible>
cd mysite/
```

* Ouvrir un fichier `build.sh`

```bash
nano build.sh
```

* Y écrire le code suivant. 

```bash
#!/bin/bash
for file in *.md
do
    export html=$(markdown -f fencedcode $file)
    filename=$(echo "$file" | sed 's|.md|.html|g')
    cat template.html | envsubst > $filename
    echo "$file"
done
```

* Ouvrir un fichier `template.html`

```bash
nano template.html
```

* Ecrire (et/ou modifier au choix) le template

```html
<!DOCTYPE html>
<html><head>
  <meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <link rel="stylesheet" href="style.css" />
</head><body>
<a href="index.html">Retour à l'index</a>
${html}
</body></html>
```

* Ajouter le code CSS (j'ai une préférence pour `classless.css`, donc j'ai copié [le code](https://classless.de/classless.css) dans le fichier et modifié le premier bloc avec mon thème (voir plus bas)

```bash
nano public/style.css
```

* Ajouter un premier fichier

```bash
nano index.md
```

* Générez le (pas besoin de refaire la commande chmod par après)

```bash
chmod +x build.sh
./build.sh
```

## Mon thème classless
Voici mon thème classless:

```css
:root {
    --rem: 15px;
    --navpos: absolute;
    --width: 800px;
    --font-p: 1.4em/1.5 et-book, Palatino, "Palatino Linotype", "Palatino LT STD", "Book Antiqua", Georgia, serif;
    --font-h: 1.4em/1.5 et-book, Palatino, "Palatino Linotype", "Palatino LT STD", "Book Antiqua", Georgia, serif;
    --font-c: .9em/1.4 "Iosevka Term Curly", monospace;
    --ornament: "";
    --border: 1px solid var(--cmed);
    --cfg: #111;
    --cbg: #fffff8;
    --cdark: #111;
    --clight: #fffff8;
    --cmed: #b4d5fe;
    --clink: #111;
    --cemph: #111;
}
```

## Extensions
### Support pour les couleurs dans les blocs de code
Simplement ajouter les lignes suivantes dans le `<head>` du template

```html
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/highlight.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/styles/default.min.css">
<script>hljs.highlightAll();</script>
```

### Support pour les formules mathématiques
Ajouter le code suivant dans le head:

```html
<script>
MathJax = {
tex: {
inlineMath: [['$', '$'], ['\\(', '\\)']]
}
};
</script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"></script>
```
