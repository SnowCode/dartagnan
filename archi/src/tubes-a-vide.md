### Les tubes à vide (1945-1955)
En 1943, le premier ordinateur digital "COLLOSUS" mais est un secret militaire.

En 1945, l'ENIAC est créé. Voir l'article Wikipedia pour se rendre compte à quel point l'ENIAC est énorme.

* [Wikipedia - ENIAC](https://fr.wikipedia.org/wiki/ENIAC) 

En 1945, John von Neumann propose une évolution de l'ENIAC appelée EDVAC, notamment pour résoudre le problème majeur de la programmation très laborieuse de l'ENIAC.

Il crée ainsi l'architecture von Neumann où la machine est controlée par un programme dont les instructions sont stoquées en mémoire, le programme pouvant modifier ses propres instructions. 

En 1949, Maurice Wilkes construit l'EDSAC, qui est le premier ordinateur basé sur l'architecture von Neumann.

En 1951 construit l'UNIVAC dont les données sont stoquées sur bande magnétiques.

En 1953, IMB lance l'IBM 701 puis ensuite le 704 et 709 et 650.

