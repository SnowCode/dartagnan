### Les circuits intégrés (1965-1970)
Les circuits intégrés sont essentiellement de très petits trasnsitors dans une petite boite. Encore une fois: moins cher, plus fiable et plus rapide

En 1958, le premier circuit imprimé par *Texas Instruments*

En 1964, première gamme d'ordinateurs par IBM qui soit compatibles entre eux mais ayant des puissances croissantes en fonction des utilisateurs (modèles 30/40/50/65). Et multiprogrammation (plusieurs programmes en mémoire) et émulation des modèles précédents (1401 et 7094) par microprogrammation.

En 1969, création de MULTICS et UNIX (qui donnera Linux et macOS)

