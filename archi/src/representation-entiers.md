### Représentation des nombres entiers
Pour faciliter la vie du programmeur (quand l'assembleur n'était pas encore très utilisé et que les données étaient manuellement codées en binaire) le système "BCD" (Binary Coded Decimal) a été créé.

A la place de coder un nombre en binaire directement : 

$$
 14\_{10} = 1110\_2 
$$

On va le codé par équivalent décimal :

$$
 14\_{10} = 0001 0100\_2 
$$

Ce système, bien que plus facile pour le programmeur est plus complexe pour le processeur, donc avec l'arrivée de l'assembleur et autres, ce système est vite abandonné.

<!-- Addition en BCD -->

