### Représentation de nombres naturels
L'espace mémoire pour la représentation d'un nombre est toujours un nombre entier d'octet, la taille dépends de la valeur représentée :

| Bits | Nom FR | Nom EN |
| ---- | --- | --- |
| 8 | Octet | Byte |
| 16 | Entier court | Short integer |
| 32 | Entier | Integer |
| 64 | Entier long | Long integer |

Si un nombre représenté laisse des emplacements libres sur la gauche, on les remplace par des 0 pour entrer dans l'emplacement mémoire prévu.

Par exemple pour représenter 5 dans un emplacement mémoire de 8 bits. On le transforme en binaire et on obtient 101 mais pour le faire entrer dans un octet cela va être 00000101.

* Pour connaitre le nombre de configurations possible de $ n $ bits :

$$
 2^n 
$$

* Pour connaitre le nombre maximal pouvant être représenté en $ n $ bits (on diminue le nombre de configuration de 1, car il y a le 0)

$$
 2^n - 1 
$$

* Pour savoir combien de bits il faut avoir pour représenter des nombres de 0 à $ x $ :

$$
 log_2(x+1) 
$$

* Pour savoir la caractéristique binaire d'un multiple d'un nombre $ n $ divisible par 2, il terminera par $ log_2(n) $ zéros.
* Comment savoir comment évolue un nombre $ n $ qui est multiplié ou divisé par un multiple de 2. Il suffit de décaler les nombres vers la droite ou vers la gauche de $ log_2(n) $ 
* Comment faire une division avec reste d'un nombre $ n $ multiple de 2. Il faut regarder les $ log_2(n) $ bits de droite (premiers), ces bits représentent le Reste, tandis que les autres bits de droites seront le Quotient.
