### Avantages et inconvénients de l'intégration à large échelle
Avantages:

* Augmentation de la capacité des puces mémoires
* Augmentation de la vitesse, performance et sophistication des composants (distances plus courte et donc fréquence d'horloge, nombre d'instructions par secondes) plus grande
* Plusieurs composants sur une seule puce

Inconvénients:

* Il n'est pas possible de réduire infiniment la taille d'un transistor, il faut un minimum d'atomes our que ce soit suffisament fiable, et les limites atomiques seront bientot atteintes.
* Une densité forte induit des phénomènes parasites et fuites de courrant
* La quantité d'énergie dissipée devient problématique (ça chauffe trop). C'est à cause de la "friction" des électrons. Les transitors libèrent de l'énergie. Et la quantité d'énergie est liée à la tension et à la fréquence. Mais la tension ne peut dessendre en dessous de 0.9V sans causer des problèmes techniques. Et la fréquence est ainsi plafonée à 5GHz.

