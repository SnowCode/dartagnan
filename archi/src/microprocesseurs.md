### Microprocesseurs (1970-???)
Toujours pareil, plus petit, moins cher, plus performant et plus fiable.

En 1971, premier microprocesseur (ayant une puissance similaire à l'ENIAC)

En 1973, création du language C pour le développement de UNIX

En 1978, création de la famille de processeur x86 par Intel. Comme une garantie que tous les processeur de cette famille soit compatibles entre eux car le jeu d'instructions est standardisé.

* [x86](https://fr.wikipedia.org/wiki/X86)

En 1981, design publique du IBM personal computer permetantde faire un stadnard au niveau de tout l'ordinateur.

En 1984, lancement du Apple Macintosh avec une interface graphique grand public.


