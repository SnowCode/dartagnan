#### La convention signe + valeur
Pour représenter des chiffres négatifs aussi bien que positif on peut réserver un bit au début de l'espace mémoire pour le signe. Si ce bit est à 1, le chiffre est négatif, sinon, il est positif.

L'un des problèmes est que les opérations arithmétiques ne sont pas facile et qu'il existe la valeur +0 et -0 qui n'ont pas de sens

* Pour savoir le nombre minimal et maximal de nombres pouvant être représenté en $n$ bits. On va de $-2^{n-1}$ à $+2^{n-1}$. On soustrait 1 car il y a maintenant un bit utilisé pour représenter le signe.
* Pour savoir le nombre maximal de chiffres pouvant être représentés. 

$$
 2^{n-1} * 2 
$$


