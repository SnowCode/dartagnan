### Représentation interne des informations
Un bit est représenté par un 0 ou 1. Allumé ou éteint.

Un octet (ou *byte* en anglais) corresponds à 8 bits. 

Un *nibble*, moins courrant que l'octet est un demi-octet (4 bits)

Les cellules mémoires sont elles aussi organisées en blocs de taille plus importante, qui n'est pas standardisée et dépends du processeur utilisé. Elle peut être de 16, 32 ou 64 bits.

Pour savoir combien de configurations peuvent exister avec $x$ nombres de bits, on peut utiliser la formule : 

$$
 2^x 
$$

Pour connaitre l'inverse, le nombre de bits nécessaire pour représenter $y$ nombre de configurations (arrondis à la hausse) :

$$
 log_2(x) 
$$

La signification d'une séquence binaire dépends du contexte. Sans ce contexte, il est impossible de savoir à quoi corresponds une séquence binaire.

Les types de variables déclarent une zone mémoire de X bits. Par exemple en java `int` corresponds à une zone mémoire de 64 bits

**Attention !** Il faut bien faire attention qu'un nombre ne peut pas être représenter si il prends plus de bits que le nombre de bits disponibles dans l'espace mémoire (exemple, 128 ne peut pas être représenté dans un espace de 7 bits). Il faut donc toujours vérifier que le nombre n'excède pas le nombre maximal.

