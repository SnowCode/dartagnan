## Deux millénaires de progrès
Vers 500av JC, nous avons commencé à compter avec des bouliers, des tables à compter et des bâtons de Neper

* [Fonctionnement d'un bâton de Napier](https://fr.wikipedia.org/wiki/B%C3%A2tons_de_Napier)

1614: John Neper invente la théorie des logarithmes. Grace aux logarithme il est possible de remplacer la multiplication ou la division de 2 nombres par l'addition ou la soustraction de leur logarithmes : $ \log{a\*b} = \log{a} + \log{b} $ et $ \log{\frac{a}{b}} = \log{a} - \log{b} $

1623: Wilhelm Shickard (mathématicien) consoit une machine à calculer qui reprends l'idée des batons de Neper

1642: Blaise Pascal (philosophe et scientifique) invente une machine à calculer qui permet principalementd'additionner et de soustraire 2 nombres de six chiffres. En répetant l'opération on pouvait ainsi multiplier.

* [Additioner avec la réplique de la Pascaline 1945](https://www.youtube.com/watch?v=GX4RQK__fQc)

1673: Gottfried Wilhelm Leibniz améliore la Pascaline pour faire de multiplications et des divisions. Mais, il invente aussi le système binaire et l'arithmétique binaire qui sont à la base des ordinateurs actuels.

En 1805 Joseph Jacquard invente le premier système de programmation. Il s'agit de bandes de carton perforées pour créer de manière automatique des motifs complexes sur un métier à tisser.

En 1833 Charles Babbage invente la machine à différence pour réaliser des tables de calculs et ensuite la *machine analytique* qui permet de réaliser différentes opérations à partir d'un programme établi sur une carte perforée. Elle était cependant irréalisable par les moyens techniques de l'époque. Mais l'idée était très novatrice, en intégrant des principes comme les programmes, le processeur, les entrées et sorties et de la mémoire.

* [The Babbage Difference Engine #2 at CHM](https://www.youtube.com/watch?v=be1EM3gQkAY)

En 1854, George Boole invente les bases mathématiques de la logique moderne. L'algèbre de Boole est à la base de la conception de circuits électroniques. C'est de Boole que viens "boolean".

* [Wikipedia - Algèbre de Boole (logique)](https://fr.wikipedia.org/wiki/Alg%C3%A8bre_de_Boole_(logique))

En 1890, Herman Hollerith construit un calculateur statistique avec des cartes perforées pour accélérer le recensement de la population américaine. 6 ans plus tard il fonde la *Tabluating Machine Company* qui deviendra finalement *International Business Machine Corporation* (IBM) en 1924.

En 1904, John Ambrose Fleming invente le premier "tube à vide" (on va voir plus tard à quoi ça sert)

En 1936, Alan Turing crée l'idée de la *machine de Turing* suseptible de résoudre tout problème calculable. Tout problème qui ne peut être calculé avec la machine de Turing est dit incalculable. Il cree les concepts d'algorithme et de calculabilité

* [Computerphile - Turing Machines Explained](https://www.youtube.com/watch?v=dNRDvLACg5Q)
* [Computerphile - Turing & The Halting Problem](https://www.youtube.com/watch?v=macM_MtS_w4)

En 1938, Claude Shannon crée la *théorie de l'information* qui fait la synthèse de nombres binaires, de l'algèbre booléenne, et de l'électronique.

* [Khan Academy - What is information theory?](https://yewtu.be/watch?v=d9alWZRzBWk)

En 1936, Konrad Zuse construuit les premiers calculateurs électromécaniques basés sur le système binaire (capable de faire une multiplication en 5 seconde)

De 1944 à 1947, Howard Aiken conçoit le Mark I. Un énorme calculateur électromécanique qui fut ensuite remplacé par le Mark II qui utilise des relais plus rapides. Ces calculateurs sont obsolètes dès leur construction car l'ère de l'électronique commence.
