### Les transistors (1955-1965)
Après les tubes à vide, il y eu les transistors. Beaucoup plus rapide, plus fiable et moins cher que les tubes.

C'est aussi à cette période que des languages plus évolués comme le FORTTRAN et le COBOL apparaissent. Ainsi que des composants comme des imprimantes ou des bandes magnétiques.

En 1960, le premier *mini-ordinateur*, le DEC PDP-1 ($120000), ainsi que le premier écran graphique et premier jeu vidéo.

En 1964, premier super ordinateur scientifique (qui introduit la notion de paralléllisme, c'est adire plusieurs unités fonctionnelles travaillant en même temps) et de coprocesseur pour s'occuper des tâches et des entrées-sorties

* [Wikipedia - Parallélisme (informatique)](https://fr.wikipedia.org/wiki/Parall%C3%A9lisme_(informatique))
* [Wikipedia - Coprocessor](https://en.wikipedia.org/wiki/Coprocessor)

En 1965, le DEC PDP-8 ($18000), premier ordinateur de masse avec 50000 exemplaires vendu. Introduit le concept de *bus* pourinterconnecter les différents éléments de l'ordinateur

* [Wikipedia - Bus (électricité)](https://fr.wikipedia.org/wiki/Bus_(%C3%A9lectricit%C3%A9))

