# Tutoriels relatif à l'infrastructure de l'école
## Se connecter au Wifi
Pour se connecter au wifi avec NetworkManager (sur le terminal, par ce que c'est toujours plus cool)

* Ajouter un nouveau réseau (remplacer `wlan0` si votre interface Wifi est différente, tu peux connaitre ton interface via `sudo ifconfig`) 

```bash
sudo nmcli con add type wifi ifname wlan0 con-name HELMo-Wifi ssid HELMo-Wifi
```

* Entrer dans l'interface de modification du réseau

```bash
sudo nmcli con edit id HELMo-Wifi
```

* Mettre la méthode ipv4 à auto, le EAP à [PEAP](https://fr.wikipedia.org/wiki/Protected_Extensible_Authentication_Protocol) (c'est la méthode d'authentification du wifi qu'utilise HELMo), la "phase 2" à [MS-CHAP](https://fr.wikipedia.org/wiki/MS-CHAP) (une méthode d'authentication de réseau en clair développée par Microsoft) et enfin [WPA-EAP](https://fr.wikipedia.org/wiki/Wi-Fi_Protected_Access) (pour les mots de passes sur ce genre de réseau)
 
```
set ipv4.method auto
set 802-1x.eap peap
set 802-1x.phase2-auth mschapv2
set wifi-sec.key-mgmt wpa-eap
```

* Ajouter les identifiant et mot de passe (matricule et mot de passe envoyé par mail après l'inscription, remplacer `MATRICULE` et `MOTDEPASSE`)

```
set 802-1x.identity MATRICULE
set 802-1x.password MOTDEPASSE
```

* Enfin, sauvegarder les changement et activer le réseau

```
save
```

* À présent le réseau devrait être accessible normalement par `nmtui` ou quelque soit le client NetworkManager utilisé.

## Se connecter au VPN
* Installer openvpn3 ([Instructions pour Debian, Ubuntu et Fedora](https://openvpn.net/cloud-docs/openvpn-3-client-for-linux/), [Package AUR pour Arch `openvpn3`](https://aur.archlinux.org/packages/openvpn3))
* Télécharger le fichier ressource OpenVPN pour MAC. 
* Extraire l'archive

```
cd Téléchargements/
unzip VpnInfoCG-2021.zip
cd VpnInfoCG-2021.tblk/
```

* Importer la configuration et se connecter (utilisateur: matricule, mot de passe: mot de passe HELMo)

```bash
openvpn3 config-import --config VpnInfoCG-2021.ovpn
openvpn3 session-start --config VpnInfoCG-2021.ovpn
```

* Vérifier que le VPN est connecté

```bash
openvpn3 sessions-list
```

* Pour se déconnecter du VPN lancer la commande suiviante

```bash
sudo pkill openvpn3
```

## Acceder au serveur
> Chaque étudiant a un utilisateur sur le serveur *dartagnan* du campus. Sur lequel on peut heberger un site ou des fichiers. Sinai que faire d'autre choses sympa. **Note**: Les CGI ne fonctionnent pas dans ce dossier, à l'exception de PHP.

### Via SSH (CLI)
* Se connecter Grace a son numéro de matricule (en minuscule) et son mot de passe

```bash
ssh MATRICULE@dartagnan.cg.helmo.be
# ou par l'intranet
ssh MATRICULE@192.168.128.13
```

### Via SFTP (avec sshfs)
* Installer sshfs (exemple ci dessous avec Debian)

```bash
sudo apt install sshfs
```

* Creer un dossier pour monter le serveur

```bash
mkdir dartagnan
```

* Monter le dossier utilisateur dans `dartagnan/` (avec son matricule en minuscule et son mot de passe)

```bash
sshfs dartagnan/ MATRICULE@dartagnan.cg.helmo.be:/home/MATRICULE
# ou par l'intranet
sshfs dartagnan/ MATRICULE@192.168.128.13:/home/MATRICULE
```

* Une fois terminé, pour se reconnecter démonter le dossier

```bash
umount dartagnan
```

### Accéder aux fichiers du dossier `public_html` depuis l'intranet
* Simplement aller sur `http://192.168.128.13/~MATRICULE` en remplaçant `MATRICULE` par son matricule en minuscule

### Accéder aux fichiers du dossier `public_html` depuis l'extérieur sans VPN
* Par défault c'est bloqué donc il faut créer un fichier `.htaccess` pour l'autoriser

```bash
cd public_html/
echo "Require all granted" >> .htaccess
```

* Le site est disponible à l'addresse `https://dartagnan.cg.helmo.be/~MATRICULE` depuis n'importe où (sauf la corée du nord)
