# Créer un serveur IRC de A à Z

Voici de quoi ce tutoriel va parler :

* Installer et configurer un IRCd (*ngircd*)
* Configurer *ngircd* pour fonctionner de manière sécurisée avec TLS
* Créer un réseau IRC sur *TOR Onion Services* (dark web)
* Configurer un client web pour le réseau (*gamja* mais la partie sur *webircgateway* est la même pour tous les clients web)

Et quelques limitations :

* Pas de régénération automatique du certificat TLS

## Installer et configurer ngircd
* Installer les programmes requis pour compiler *ngircd*

```bash
sudo apt install letsencrypt gnutls-build automake build-essential git
```

* Télécharger le code source

```bash
git clone https://github.com/ngircd/ngircd
cd ngircd/
```

* Compiler *ngircd*

```bash
./autogen.sh
./configure --with-gnutls
sudo make install
```

* Copier l'exemple de configuration (et le modifier comme on veux)

```bash
cp doc/sample-ngircd.conf.tmpl ngircd.conf
nano ngircd.conf
```

* Lancer *ngircd* (retirer le `-n` une fois que ça fonctionne pour le faire tourner en arrière plan

```bash
ngircd -f ngircd.conf -n
```

## Configurer TLS sur *ngircd* (pas nécessaire si on utilise TOR)
* Générer un certificat TLS avec *letsencrypt* (remplacer le nom de domaine et remplacer nginx par le serveur web en place)

```bash
sudo systemctl stop nginx 
sudo certbot certonly --standalone -d TON.NOM.DE.DOMAINE
sudo systemctl start nginx
sudo cp /etc/letsencrypt/live/TON.NOM.DE.DOMAINE/fullchain.pem cert.pem
sudo cp /etc/letsencrypt/live/TON.NOM.DE.DOMAINE/privkey.pem key.pem
certtool --generate-dh-params --bits 4096 --outfile dhparams.pem
sudo chown $USER:$USER *.pem
```

* Ajoute le bloc de configuration suivante dans `ngircd.conf`

```toml
[SSL]
    CertFile = cert.pem
    CipherList = SECURE128:-VERS-SSL3.0
    DHFile = dhparams.pem
    KeyFile = key.pem
    Ports = 6697, 9999
```

* Relancer *ngircd*

```bash
pkill ngircd
ngircd -f ngircd.conf -n
```

## Créer un service TOR Onion
* Changer le paramètre `Listen` de `[Global]` à `127.0.0.1` et le `Ports` à `6667` dans `ngircd.conf`
* Installer TOR

```bash
sudo apt install tor
```

* Ajouter un service caché pour `127.0.0.1:6667`

```bash
sudo echo "HiddenServiceDir /var/lib/tor/irc_hidden_service/" >> /etc/tor/torrc
sudo echo "HiddenServicePort 6667 127.0.0.1:6667" >> /etc/tor/torrc
```

* Lancer TOR

```bash
sudo systemctl restart tor
```

* Obtenir l'addresse `.onion`

```bash
sudo cat /var/lib/tor/irc_hidden_service/hostname
```

## Configurer les services IRC Anope
* Télécharger le code source d'Anope

```bash
git clone https://github.com/anope/anope
cd anope/
```

* Compiler Anope

```bash
./Config
cd build
make
make install
```

* Copier les fichiers de configuration pour les services et pour NickServ

```bash
cd /chemin/vers/l'installation/de/anope
cp conf/example.conf conf/services.conf
cp conf/nickserv.example.conf conf/nickserv.conf
nano conf/services.conf
```

* Regarde pour chaque bloc de la configuration qu'il soit comme ceux si dessous :

```
define
{
    name = "services.host"
    value = "services.irc.net"
}

uplink
{
    host = "127.0.0.1"
    port = 6667
    password = "123abc"
}

serverinfo
{
    name = "services.irc.net"
}

# Load ngircd protocol module
module
{
    name = "ngircd"
}

networkinfo
{
    # Must be set to the "MaxNickLength" setting of ngircd!
    nicklen = 9

    # When not using "strict mode", which is the default:
    userlen = 20

    chanlen = 50
}
```

* Ouvre `conf/nickserv.conf` et fait la même chose que ci dessus avec ceci:

```
module
{
    name = "nickserv"

    # not required if you are running ngircd with a higher nickname limit
    # ("MaxNickLength") than 11 characters, but REQUIRED by default!
    guestnickprefix = "G-"
}
```

* Change ces paramètres de `ngircd.conf` 

```toml
[GLOBAL]
     Name = server.irc.net
     Ports = 6667

[SERVER]
     Name = services.irc.net
     MyPassword = 123abc
     PeerPassword = 123abc
     ServiceMask = *Serv
```

* Relance *ngircd* et lance Anope

```bash
pkill ngircd
ngircd -f /chemin/vers/ngircd.conf -n
bin/services
```

## Configurer le client web (gamja avec webircgateway)
* Dans la configuration *ngircd*, changer le paramètre suivant (sinon il y aura seulement 5 personnes qui pourront utiliser le client web en même temps)

```toml
MaxConnectionsIP = 0
```

* Télécharge le code de *webircgateway* et compile le

```bash
git clone https://github.com/kiwiirc/webircgateway
cd webircgateway
go build
```

* Copier et modifier la configuration 

```bash
cp config.conf.example config.conf
nano config.conf
```

* Change les paramètres suivant 

```toml
[fileserving]
enabled = true
webroot = gamja/dist

[server.1]
bind = "0.0.0.0"
port = 80

[upstream.1]
hostname = "127.0.0.1"
port = 6667
tls = false
```

* Télécharge le code de *gamja* (requiers une version récente de Node et de NPM), puis compiler gamja et créer les fichiers de configuration

```bash
git clone https://git.sr.ht/~emersion/gamja
cd gamja
npm install --include=production
npm run build
nano config.json
```

* Colle la configuration suivante

```json
{
    // IRC server settings.
    "server": {
        // WebSocket URL or path to connect to (string).
        "url": "wss://127.0.0.1",
        // Channel(s) to auto-join (string or array of strings).
        "autojoin": "#gamja",
        // Controls how the password UI is presented to the user. Set to
        // "mandatory" to require a password, "optional" to accept one but not
        // require it, "disabled" to never ask for a password, or "external" to
        // use SASL EXTERNAL. Defaults to "optional".
        "auth": "optional",
        // Default nickname (string).
        "nick": "asdf",
        // Don't display the login UI, immediately connect to the server
        // (boolean).
        "autoconnect": true,
        // Interval in seconds to send PING commands (number). Set to 0 to
        // disable. Enabling PINGs can have an impact on client power usage and
        // should only be enabled if necessary.
        "ping": 60
    }
}
```

* Lancer *webircgateway*

```bash
cd ..
./webircgateway --config=config.conf
```

* Ajoute `?server=/webirc/websocket/` à l'URL que *webircgateway* utilise
