# Notes de développement web
## Ressources et liens
* [Tutoriels helmo](https://dartagnan.cg.helmo.be/~p150107/tutoriels/index.php)
* [Introduction à Internet](./Introduction_Internet.pdf) (PDF)

## Web et internet
* [Wikipedia - Internet](https://fr.wikipedia.org/wiki/Internet)

Internet signifie *interconnected networks*. C'est un réseau informatique mondial accessible au publique qui est lui même un regroupement de pleins de réseaux qui sont eux même constitués de réseaux...

* [Wikipedia - World Wide Web](https://en.wikipedia.org/wiki/World_Wide_Web#HTML)

Le web est un système d'information qui permet d'accéder à beaucoup de ressources (données) via internet via des liens.

Les réseaux ont de nombreux avantages, y compris :

* Partager des ressources (données)
* Fiabilité via duplication
* Adaptabilité (il est facile de changer les ressources accessible)
* Collaboration

## Protocoles
* [Wikipedia - Protocole informatique](https://fr.wikipedia.org/wiki/Protocole_informatique)

Un protocole est un ensemble de règles qui régissent les échanges de données dans un réseau. C'est le language qui permet à plusieurs ordinateurs de communiquer. 

* [Wikipedia - Modèle OSI](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI)
* [Cookie connecté - Comprendre les modèles OSI et TCP/IP](https://www.youtube.com/watch?v=26jazyc7VNk)

Il existe un modèle théorique pour expliquer le fonctionnement des protocoles en plusieurs niveau. Ce modèle est le modèle OSI (Open Systems Interconnection model)

![wikipedia OSI](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/OSI_Model_v1.svg/langfr-800px-OSI_Model_v1.svg.png)

## Quelques mots de vocabulaire
* [Wikipedia - Modem](https://fr.wikipedia.org/wiki/Modem)

Un *modem* (qui signifie *modulateur-démodulateur*) est un appareil (qui n'est plus utilisé aujourd'hui) qui sert à relier un ordinateur à un réseau analogique, comme le réseau téléphonique. 

* [Wikipedia - Routeur](https://fr.wikipedia.org/wiki/Routeur)

Un *routeur* est un appareil qui permet de router des paquets. C'est a dire de faire transiter les paquets d'une interface réseau à une autre. (couche 3 du modèle OSI)

* [Wikipeida - Network Switch](https://en.wikipedia.org/wiki/Network_switch)

Un *commutateur* (ou *switch*) est un appareil qui relie plusieurs appareils sur le réseau et qui transmets les paquets d'un à l'autre ainsi qu'a l'extérieur (via un routeur). Ils opèrent sur la deuxième couche OSI, sauf si un routeur est intégré dans lequel il opère aussi dans 3e couche OSI. Le commutateur crée ainsi lui même son propre réseau local. 

* [Wikipedia - Domain Name System](https://fr.wikipedia.org/wiki/Domain_Name_System)

Le DNS est un service informatique distribué (non centralisé) utilisé qui traduit des noms de domaines en adresses IPs ou autres enregistrements. C'est une sorte d'annuaire téléphonique pour internet. A la place d'écrire `8.8.8.8` pour aller sur Google, vous pouvez juste écrire `google.com`. Cela est fait grâce au serveur DNS.

* [Wikipedia - Nom de domaine](https://fr.wikipedia.org/wiki/Nom_de_domaine)

Les *noms de domaines* (ou NDD ou encore DN) est un identifiant de domaine internet. Un domaine est un ensemble d'ordinateurs reliés à internet possédant une caractéristique commune. Par exemple le domaine `.be` pour la Belgique (c'est plus compliqué que ça mais c'est pas grave pour le moment). 

Un nom de domaine comme `.be` est ce qui s'appelle un domaine de premier niveau (ou TLD). 

Mais il peut aussi y avoir des noms de domaines des second niveau (SLD) tel que `helmo.be` ou encore `cg.helmo.be`, ce site (`dartagnan.cg.helmo.be`) l'utilise. 

* [Wikipedia - Fournisseur d'accès à Internet](https://fr.wikipedia.org/wiki/Fournisseur_d%27acc%C3%A8s_%C3%A0_Internet)

Un FAI (ou fournisseur d'accès à Internet, ou encore ISP en Anglais) est un organisme (le plus souvent une entreprise) qui offre une connection à Internet (exemple, Proximus, Orange, etc). 

## Histoire d'internet
* [Wikipedia - Histoire d'internet](https://fr.wikipedia.org/wiki/Histoire_d%27Internet)

* 1955: Prémise d'un réseau commercial par IBM
* 1958: Réseau militaire SAGE par l'ARPA 
* 1961: Création du premier modem
* 1961: Première théorie de communication par commutation de paquets en remplacement des circuits dédiés
* 1969: 1er réseau de commutation de paquets *ARPANET*, relie 4 ordinateurs situés dans 4 universités américaines grâce aux commutateurs (switch) IMP
* 1972: Première application de courrier électronique
* 1973: Définition du protocole TCP/IP
* 1974: ARPANET adopte le protocole TCP/IP et crée une passerelle avec un autre réseau CSNET (premiere mention d'*internet*)
* 1984: Fondation de Cisco Systems qui fabriqueles premiers routeurs et premiers serveur DNS

## TCP/IP
* [Computerphile - Network Stacks and the Internet](https://youtube.com/watch?v=PG9oKZdFb7w)
* [Wikipedia - Internet Protocol Suite](https://en.wikipedia.org/wiki/Network_switch)

La *suite des protocoles internet* (aussi appellé **TCP/IP**) est l'ensemble des protocoles utilisé pour le transfert de données sur internet. Les protocoles fondamentaux sont les protocoles **TCP**, **IP** et **UDP**

* [Wikipedia - Transmission Control Protocol](https://en.wikipedia.org/wiki/Transmission_Control_Protocol)

Le protocole TCP (Transmission Control Protocol) permet de créer un flux de données ordoné, fiable et sans erreurs entre plusieurs appareils opérant sur le réseau IP. (couche 4 de OSI)

* [Wikipedia - Internet Protocol](https://fr.wikipedia.org/wiki/Internet_Protocol)
* [Computerphile - IP Addresses and the Internet](https://www.youtube.com/watch?v=L6bDA5FK6gs)

Le protocol internet (IP) est une famille de protocoles de communication de réseaux pour internet. Ils sont de niveau 3 dans le modèle OSI et permet un service d'éaddressage unique pour l'ensemble des appareils connectés. Le protocole IP encapsule les données (segments) en "paquets". 

Le protocole a 2 versions utilisées aujourd'hui. L'IPv4 et l'IPv6. Les adresses IPv4 sont codées en 32bits (soit 4 294 967 296 adresses possibles) est est formatté de cette manière: `193.190.64.124`. Le problème étant que la demande est trop forte par rapport à la quantité d'adresses possibles. Ainsi un successeur à l'IPv4 a été créé, c'est l'IPv6.

L'IPv6 est codée en 128bits (soit environ $3.4 \* 10\^{38}$ adresses possibles) et fonctionne sous le format `2001:41d0:404:200::597`.

* [Wikipedia - User Datagram Protocol](https://fr.wikipedia.org/wiki/User_Datagram_Protocol)

Le protocol UDP (User Datagram Protocol) est également un protocole qui permet de créer un flux de donnée comme *TCP*, sauf que contrairement à TCP, il n'y a pas de véirification de la fiabilité ou de la réception des paquets. Les paquets sont juste envoyés continuellement ce qui permet d'avoir un flux plus rapide qu'avec TCP. C'est nottament ce qui est utilisé pour le streaming sur internet, les logiciels de vidéoconférence, les jeux en ligne et certains VPNs.


## Une multitude d'autres protocoles
Voici une petite liste d'autres protocoles et leur utilisation:

* SMTP (Simple Mail Transfer Protocol)

| Acronyme | Nom | Port | Utilisation |
| --- | --- | --- | --- |
| HTTP | Hyper Text Transfer Protocol | 80 | Protocol principale du web |
| SMTP | Simple Mail Transfer Protocol | 25 | Envois de mails |
| POP | Post Office Protocol | 110 | Récupération de mails (stoqué hors-ligne sur l'appareil) | 
| IMAP | Internet Message Access Protocol | 143 | Récupération de mails, mais avec synchronisation | 
| FTP | File Transfer Protocol | 21 | Transfers de fichiers entre un client et un serveur |
| WebDAV | Web-based Distributed Authoring and Versioning | N/A | Gestion de fichiers complète avec synchronisation et publication sur le serveur | 
| LDAP | Lightweight Directory Access | 389 | Gestion de services d'annuaire |
| SOAP | Simple Object Access Protocol | N/A | Échange d'informations structurées en XML via HTTP ou SMTP |
| Telnet | Telnet | 23 | Accès à une machine à distance par ligne de commande |
| VoIP | Voice Over IP | N/A | Transport de voix sur le réseau TCP/IP |
| Streaming | Streaming | N/A | Lecture d'un flux audio/vidéo ) mesure qu'il est diffusé |

Et bien d'autres...

## Sécurité et chiffrement
* [Computerphile - Transport Layer Security (TLS)](https://www.youtube.com/watch?v=0TLDTodL7Lc)
* [Wikipedia - Transport Layer Security](https://fr.wikipedia.org/wiki/Transport_Layer_Security)

Au début d'internet, très peu de choses était chiffrée. Donc SSL a commencé à être développé en 1994 avant de devenir TLS en 1999. Cette couche de sécurité se met entre la couche de transport (TCP) et un autre protocole (comme HTTP ou Telnet par exemple). 

Ainsi la combinaison de TLS avec d'autre protocole donne des noms différents :

* HTTP + TLS = HTTPS
* Telnet + TLS = SSH

## URL et URI
* [Wikipedia - Uniform Resource Locator](https://fr.wikipedia.org/wiki/Uniform_Resource_Locator)

Une URL (ou adresse web) est une chaine de caractère qui permet d'identifier une ressource sur le web par son emplacement, de préciser le protocole internet utilisé ainsi que le format de données. 

```
[protocole]://[nom de domaine]/[chemin de fichier]
https://dartagnan.cg.helmo.be/~q220244/webdev.html
```

* [Wikipedia - Uniform Resource Identifier](https://fr.wikipedia.org/wiki/Uniform_Resource_Identifier)

Les URLs font partie d'un plus grand ensemble, les URI (Uniform Resource Identifier). Voici un exemple d'URI dans le contexte de SFTP (SSH File Transfer Protocol)

```
[protocole]://[utilisateur]@[hôte]:[chemin]
sftp://q220244@dartagnan.cg.helmo.be:/home/q220244/public_html
```

Il peut aussi y avoir des adresses dites *relatives* contrairement aux adresses dites *absolues* (tout comme avec les chemins de fichiers)

* Chemin de fichier absolu : `/home/q220244/public_html/devweb.html`
* Chemin de fichier relatif (à l'emplacement utilisateur *home*) : `./public_html/devweb.html` ou `public_html/devweb.html`

Un chemin absolu est en vérité un fichier relatif à la racine du disque (dans le cas de Windows, la racine est `C:\` tandis que dans le cas de macOS ou Linux la racine est `/`)


## Sous-réseau et masque de sous réseau
> *uhh. Je pense que je suis parti beaucoup trop loin dans cette section, mais c'est cadeau. Merci beaucoup à @Yannex et les notes de cours de Swilagod pour m'avoir aidé à comprendre...*

* [Wikipedia - Sous-réseau](https://fr.wikipedia.org/wiki/Sous-r%C3%A9seau)

Un sous réseau sert à subdiviser un réseau plus grand. Et le sous réseau est identifié par une adresse particulière qui permet de déterminer si deux machines sont directement connectées.

Il y a ainsi un "masque de sous réseau", qui permet de savoir combien d'IP peuvent exister dans ce réseau.

Voici un exemple de masque de sous réseau (mon réseau de chez moi en l'occurence) :

```
255.255.255.0
```

Pour faire plus de sens il faut convertir cette adresse en binaire, dans quel cas on a ceci:

```
11111111.11111111.11111111.00000000
```

On peut voir qu'il y a ainsi 24 bits 1 (ce sont des bits fixes). Et 8 bits 0 (ce sont les bits modifiables). 

Pour savoir le nombre d'adresses IP qui peuvent exister dans ce sous-réseau il faut faire 2^8 (pour les 8 bits modifiables) ce qui nous donne 256 adresses possibles.

Si on combine l'adresse de masque de sous réseau avec une adresse IP qui fait partie du sous-réseau, on peut trouver quel est l'adresse du sous-réseau (voici un exemple avec mon adresse locale `192.168.1.51`)

```
192.168.1.51 + 255.255.255.0 = adresse de mon sous-réseau
```

Traduisons ceci en binaire :

```
  11000000.10101000.00000001.00110011
& 11111111.11111111.11111111.00000000
= 11000000.10101000.00000001.00000000
```

Qui une fois retraduite en décimal donne :

```
192.168.1.0
```

Qui est l'adresse de mon sous-réseau. En revanche ce n'est pas à cette adresse que requêtes sont envoyés.

* [Adresse IP > Agrégation des adresses](https://fr.wikipedia.org/wiki/Adresse_IP#Agr%C3%A9gation_des_adresses)

Il existe une notation bien plus courte pour noter une gamme d'adresses IPs qui est la notation *CIDR*. Voici un exemple avec mon sous réseau:

```
192.168.1.0/24
```

La mention du `/24` signifie qu'il y a 24 bits fixes. Ce qui nous ramène donc à notre masque de sous réseau de départ.

Donc cette adresse correspond à la plage d'adresse IPs `192.168.1.0` jusqu'a `192.168.1.255`.
