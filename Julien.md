# A propos
## Liens
* [Mon Git](https://codeberg.org/SnowCode)
* [Mon autre site](https://wiki.snowcode.ovh/)
* [Mon compte Peertube](https://peertube.fr/c/chopin_42/videos)

## Me contacter
* Discord: `@Snow196883#8038`
* XMPP: `snowcode@jabber.fr`
* Mail: `ju.lejoly [at] student.helmo.be`
