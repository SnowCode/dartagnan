# Architecture des ordinateurs
> **Attention!** ceci sont des notes que je fais au fur et à mesure, je ne suis pas tout à fait sûr de ce que je fais. 

## Ressources et liens
* [Mathématiques - L'histoire des nombres](https://www.youtube.com/watch?v=UddhSCezAAk) explique l'histoire des nombres et des chiffres ainsi que différentes bases

## Histoire de l'informatique
Dès le début de l'humanité, nous avons commencé à compter. D'abord on a compté en **base 1** (base unitaire). Par exemple, 1 mouton = 1 caillou (d'où *calculus* qui donne le mot *calcul* en français)

> Un *nombre* est une quantité que l'on veut représenter, tandis qu'un *chiffre* est un symbole qui va être utiliser pour représenter le nombre.

Ensuite on a commencé à utiliser la *numérotation de position*. C'est toujours ce que l'on utilise aujourd'hui, c'est à dire que la position d'un chiffre dans l'écriture d'un nombre a de l'importance. 

Par exemple, en **base 10** (ce que l'on utilise au quotidien pour compter), si je prends le nombre "542", on peut le décomposer comme ceci :

$$
542 = 5\*10^2 + 4\*10^1 + 2\*10^0
$$


C'est ainsi que le *zéro* fait son apparition, avant représenter "rien" n'avait pas d'utilité, mais en numérotation de position, ça a une utilité très claire d'éviter la confusion.

$$
1 \  1 \neq 11 \neq 101 \neq 1001 \neq 1010 
$$

Voici quelques exemples de bases (celle indiquées en gras sont très liées à l'informatique)


| Base | Nom | Usage | Origine |
| --- | --- | --- | --- |
| 1 | Unitaire | Comptage (doigts, cailloux, entailles, etc) |  |
| **2** | **Binaire** | **Logique, électronique, informatique** |  |
| 5 | Quinaire | | Aztèques (doigts d'une main) |
| 7 | Septénaire | Jours de la semaine, notes (tons) | | 
| **8** | **Octal** | **Informatique** | **Premiers ordinateurs** |
| 10 | Décimal | Système le plus répandu | Chinois (doigts des 2 mains) | 
| 12 | [Duodécimal](https://www.youtube.com/watch?v=U6xJfP7-HCc) | Mois, heures, musique (ton et demi-tons) | Egyptiens |
| **16** | **Hexadécimal** | **Informatique** | | 
| 20 | Vicésimal |  | Mayas (doigts + orteils) |
| 60 | sexagésimal | Trigonométrie (angles), minutes, secondes | Babyloniens, indiens, arabes, ... |

## Deux millénaires de progrès
Vers 500av JC, nous avons commencé à compter avec des bouliers, des tables à compter et des bâtons de Neper

* [Fonctionnement d'un bâton de Napier](https://fr.wikipedia.org/wiki/B%C3%A2tons_de_Napier)

1614: John Neper invente la théorie des logarithmes. Grace aux logarithme il est possible de remplacer la multiplication ou la division de 2 nombres par l'addition ou la soustraction de leur logarithmes : $\log{a\*b} = \log{a} + \log{b}$ et $\log{\frac{a}{b}} = \log{a} - \log{b}$

1623: Wilhelm Shickard (mathématicien) consoit une machine à calculer qui reprends l'idée des batons de Neper

1642: Blaise Pascal (philosophe et scientifique) invente une machine à calculer qui permet principalementd'additionner et de soustraire 2 nombres de six chiffres. En répetant l'opération on pouvait ainsi multiplier.

* [Additioner avec la réplique de la Pascaline 1945](https://www.youtube.com/watch?v=GX4RQK__fQc)

1673: Gottfried Wilhelm Leibniz améliore la Pascaline pour faire de multiplications et des divisions. Mais, il invente aussi le système binaire et l'arithmétique binaire qui sont à la base des ordinateurs actuels.

En 1805 Joseph Jacquard invente le premier système de programmation. Il s'agit de bandes de carton perforées pour créer de manière automatique des motifs complexes sur un métier à tisser.

En 1833 Charles Babbage invente la machine à différence pour réaliser des tables de calculs et ensuite la *machine analytique* qui permet de réaliser différentes opérations à partir d'un programme établi sur une carte perforée. Elle était cependant irréalisable par les moyens techniques de l'époque. Mais l'idée était très novatrice, en intégrant des principes comme les programmes, le processeur, les entrées et sorties et de la mémoire.

* [The Babbage Difference Engine #2 at CHM](https://www.youtube.com/watch?v=be1EM3gQkAY)

En 1854, George Boole invente les bases mathématiques de la logique moderne. L'algèbre de Boole est à la base de la conception de circuits électroniques. C'est de Boole que viens "boolean".

* [Wikipedia - Algèbre de Boole (logique)](https://fr.wikipedia.org/wiki/Alg%C3%A8bre_de_Boole_(logique))

En 1890, Herman Hollerith construit un calculateur statistique avec des cartes perforées pour accélérer le recensement de la population américaine. 6 ans plus tard il fonde la *Tabluating Machine Company* qui deviendra finalement *International Business Machine Corporation* (IBM) en 1924.

En 1904, John Ambrose Fleming invente le premier "tube à vide" (on va voir plus tard à quoi ça sert)

En 1936, Alan Turing crée l'idée de la *machine de Turing* suseptible de résoudre tout problème calculable. Tout problème qui ne peut être calculé avec la machine de Turing est dit incalculable. Il cree les concepts d'algorithme et de calculabilité

* [Computerphile - Turing Machines Explained](https://www.youtube.com/watch?v=dNRDvLACg5Q)
* [Computerphile - Turing & The Halting Problem](https://www.youtube.com/watch?v=macM_MtS_w4)

En 1938, Claude Shannon crée la *théorie de l'information* qui fait la synthèse de nombres binaires, de l'algèbre booléenne, et de l'électronique.

* [Khan Academy - What is information theory?](https://yewtu.be/watch?v=d9alWZRzBWk)

En 1936, Konrad Zuse construuit les premiers calculateurs électromécaniques basés sur le système binaire (capable de faire une multiplication en 5 seconde)

De 1944 à 1947, Howard Aiken conçoit le Mark I. Un énorme calculateur électromécanique qui fut ensuite remplacé par le Mark II qui utilise des relais plus rapides. Ces calculateurs sont obsolètes dès leur construction car l'ère de l'électronique commence.

## Evolution des ordinateurs
### Les tubes à vide (1945-1955)
En 1943, le premier ordinateur digital "COLLOSUS" mais est un secret militaire.

En 1945, l'ENIAC est créé. Voir l'article Wikipedia pour se rendre compte à quel point l'ENIAC est énorme.

* [Wikipedia - ENIAC](https://fr.wikipedia.org/wiki/ENIAC) 

En 1945, John von Neumann propose une évolution de l'ENIAC appelée EDVAC, notamment pour résoudre le problème majeur de la programmation très laborieuse de l'ENIAC.

Il crée ainsi l'architecture von Neumann où la machine est controlée par un programme dont les instructions sont stoquées en mémoire, le programme pouvant modifier ses propres instructions. 

En 1949, Maurice Wilkes construit l'EDSAC, qui est le premier ordinateur basé sur l'architecture von Neumann.

En 1951 construit l'UNIVAC dont les données sont stoquées sur bande magnétiques.

En 1953, IMB lance l'IBM 701 puis ensuite le 704 et 709 et 650.

### Les transistors (1955-1965)
Après les tubes à vide, il y eu les transistors. Beaucoup plus rapide, plus fiable et moins cher que les tubes.

C'est aussi à cette période que des languages plus évolués comme le FORTTRAN et le COBOL apparaissent. Ainsi que des composants comme des imprimantes ou des bandes magnétiques.

En 1960, le premier *mini-ordinateur*, le DEC PDP-1 ($120000), ainsi que le premier écran graphique et premier jeu vidéo.

En 1964, premier super ordinateur scientifique (qui introduit la notion de paralléllisme, c'est adire plusieurs unités fonctionnelles travaillant en même temps) et de coprocesseur pour s'occuper des tâches et des entrées-sorties

* [Wikipedia - Parallélisme (informatique)](https://fr.wikipedia.org/wiki/Parall%C3%A9lisme_(informatique))
* [Wikipedia - Coprocessor](https://en.wikipedia.org/wiki/Coprocessor)

En 1965, le DEC PDP-8 ($18000), premier ordinateur de masse avec 50000 exemplaires vendu. Introduit le concept de *bus* pourinterconnecter les différents éléments de l'ordinateur

* [Wikipedia - Bus (électricité)](https://fr.wikipedia.org/wiki/Bus_(%C3%A9lectricit%C3%A9))

### Les circuits intégrés (1965-1970)
Les circuits intégrés sont essentiellement de très petits trasnsitors dans une petite boite. Encore une fois: moins cher, plus fiable et plus rapide

En 1958, le premier circuit imprimé par *Texas Instruments*

En 1964, première gamme d'ordinateurs par IBM qui soit compatibles entre eux mais ayant des puissances croissantes en fonction des utilisateurs (modèles 30/40/50/65). Et multiprogrammation (plusieurs programmes en mémoire) et émulation des modèles précédents (1401 et 7094) par microprogrammation.

En 1969, création de MULTICS et UNIX (qui donnera Linux et macOS)

### Microprocesseurs (1970-???)
Toujours pareil, plus petit, moins cher, plus performant et plus fiable.

En 1971, premier microprocesseur (ayant une puissance similaire à l'ENIAC)

En 1973, création du language C pour le développement de UNIX

En 1978, création de la famille de processeur x86 par Intel. Comme une garantie que tous les processeur de cette famille soit compatibles entre eux car le jeu d'instructions est standardisé.

* [x86](https://fr.wikipedia.org/wiki/X86)

En 1981, design publique du IBM personal computer permetantde faire un stadnard au niveau de tout l'ordinateur.

En 1984, lancement du Apple Macintosh avec une interface graphique grand public.


## L'évolution des performances 
A partir des années 70s, avec l'intégration toujours plus poussée des composants sur une puce et l'augmentation de la capacité de calcul

En 1965 Gordon Moore, constate que depuis 1959, la complexité des circuits intégrés à base de transistors double tous les ans à coût constant. Et il postule que cela va continuer ainsi; c'est la première loi de Moore.

En 1975, il revoit sa loi, il précisa sa loi en disant que le nombre de transitors sur une puce de silicium (microprocesseurs, mémoires) doublera tous les deux ans. C'est la deuxième loi de Moore. Qui s'est avérée être assez proche de la réalité jusqu'a 2010. 

### Avantages et inconvénients de l'intégration à large échelle
Avantages:

* Augmentation de la capacité des puces mémoires
* Augmentation de la vitesse, performance et sophistication des composants (distances plus courte et donc fréquence d'horloge, nombre d'instructions par secondes) plus grande
* Plusieurs composants sur une seule puce

Inconvénients:

* Il n'est pas possible de réduire infiniment la taille d'un transistor, il faut un minimum d'atomes our que ce soit suffisament fiable, et les limites atomiques seront bientot atteintes.
* Une densité forte induit des phénomènes parasites et fuites de courrant
* La quantité d'énergie dissipée devient problématique (ça chauffe trop). C'est à cause de la "friction" des électrons. Les transitors libèrent de l'énergie. Et la quantité d'énergie est liée à la tension et à la fréquence. Mais la tension ne peut dessendre en dessous de 0.9V sans causer des problèmes techniques. Et la fréquence est ainsi plafonée à 5GHz.

### Sortir de cette impasse
* Changement radical de technologie
    * Remplacement du Silicium par de l'arséniure de gallium
    * Supraconducteurs (mais nécessite une température *extrènement froide*)
    * Composants optiques
    * Dispositifs quantiques
* A court terme
    * Multiprocesseurs (plusieurs coeurs et processeurs interconnectés)
    * Fermes de serveur (architectures en parallèles distribuées)
    * Programmation parallèle efficace

## Présentation générale d'un ordinateur
Un ordinateur est une machine automatique de traitement de l'information obéissant à des programmes (suites d'instructions arithmétiques et logiques). Il y a donc la partie matérielle et la partie logicielle.

* L'**unité centrale** (processeur, mémoire centrale, gestionaire d'entrée sorties)
* Les **périphériques d'entrée** permettent d'acquérir des informations (par clavier, souris, scanner, etc)
* Les **périphériques de sortie** permettent de restituer des informations (par clavier, souris, scanner, etc)
* Des **périphériques de stockage** (mémoire secondaire) internes ou externes (ex. disque dur, lecteur-graveur CD, clé USB, etc)
* Les **périphériques de communication** soit les cartes réseau (éthernet, bluetooth, wifi) ou les modems (téléphonique, ADSL, RNIS)

Il y a différentes gammes d'ordinateurs (allant du moins cher au plus cher et évolué)

* Les **ordinateurs jetables** qui sont des puces très simples et coutant moins d'un euro produites en très grandes série pour un usage précis (exemple, carte musicale, puce RFID)
* Les **ordinateurs embarqués** est un ordinateur inclu dans un autre appareil dans le but de le piloter (électroménager, MP3, TV, scanner, pace maker, jeux, armement, distributeurs, etc). Ces ordinateurs coûtent en général quelques EUR.
* Les **téléphones intelligents et consoles de jeu** est une catégorie qui se rapproche beaucoup de l'ordinateur classique maus qui peut être réserver à des usages plus précis et à être moins évolutif dans ses performances et gamme de prix.
* Les **ordinateurs personnels**, soit les PC, en général utilisé par un utilisateur à la fois, la gamme de prix varie plus (ex tours, portables, tablettes)
* Les **serveurs** est un ordinateur prévu pour servir des services à plusieurs utilisateurs (stockage de fichier, calcul, serveur Web, etc). En général on entend par là des ordinateurs plus puissant que des ordinateurs personnels, mais il peu aussi il y avoir des "miniserveurs" qui soit tout autant puissant ou moins puissant (ancien ordinateur portable, raspberry pi, etc)
* Les **cluster** (ou **ferme de serveurs** ou **data center**) sont prévu pour fournir une puissance de calcul et/ou une grande capacité de stockage en connectant plusieurs serveurs entre eux pour se répartir les taches.
* Les **mainframes** (ou **super-ordinateurs**) avais des architectures très spécifiques, prévu pour un usage très particulier avec une puissance de calcul très grande (qui sont maintenant remplacé par les data centers qui sont plus performant et coute moins cher). En revanche il existe encore des mainframes, principalement dans le secteur bancaire ou une migration serait trop couteuse.

Le **cloud** n'est rien d'autre qu'un ou plusieurs serveurs ou fermes de serveurs qui fournissent des services à travers internet. L'interet est de mutualiser des ressources, rendre l'information accessible de n'importe où et de synchroniser les données à un moindre coût.

Il existe différents types de *services cloud* :

* Le **IaaS** (Infrastructure as a Service) qui comprends le réseau, le serveur + stockage et la virtualisation (souvent ce ne sont pas des ordinateurs dédiés mais des machines virtuelles tournant sur de gros ordinateurs)
* Le **PaaS** (Platform as a Service) qui comprends réseau, serveur + stockage, virtualisation mais aussi le système d'exploitation et les logiciels de bases (par exemple, serveur web)
* Le **SaaS** (Software as a Service) qui comprends réseau, serveur + stockage, virtualisation, système d'exploitation, logiciel de base, et logiciel cible (par exemple webapp) déjà installé pret à être utilisé. 

Maintenant pour comprendre un peu plus dans les détails comment un ordinateur monoprocesseur fonctionne, comme vu précédemment avec l'architecture de Von Neumann nous avons :

* Une unité centrale (CPU) avec une unité de controle, une unité arithmétique et logique (ALU) et un système de registre (mémoires rapides pour l'ALU)

Mais maintenant il y a aussi le principe de bus de communication (introduit par le PDP-8) qui permet d'interconnecter les composants de l'ordinateurs. 

## Le codage des informations
<!-- Il faut vraiment que je me bouge le cul putain -->

### Représentation interne des informations
Un bit est représenté par un 0 ou 1. Allumé ou éteint.

Un octet (ou *byte* en anglais) corresponds à 8 bits. 

Un *nibble*, moins courrant que l'octet est un demi-octet (4 bits)

Les cellules mémoires sont elles aussi organisées en blocs de taille plus importante, qui n'est pas standardisée et dépends du processeur utilisé. Elle peut être de 16, 32 ou 64 bits.

Pour savoir combien de configurations peuvent exister avec $x$ nombres de bits, on peut utiliser la formule : 

$$
2^x
$$

Pour connaitre l'inverse, le nombre de bits nécessaire pour représenter $y$ nombre de configurations (arrondis à la hausse) :

$$
log_2(x)
$$

La signification d'une séquence binaire dépends du contexte. Sans ce contexte, il est impossible de savoir à quoi corresponds une séquence binaire.

Les types de variables déclarent une zone mémoire de X bits. Par exemple en java `int` corresponds à une zone mémoire de 64 bits

**Attention !** Il faut bien faire attention qu'un nombre ne peut pas être représenter si il prends plus de bits que le nombre de bits disponibles dans l'espace mémoire (exemple, 128 ne peut pas être représenté dans un espace de 7 bits). Il faut donc toujours vérifier que le nombre n'excède pas le nombre maximal.

### Représentation des nombres entiers
Pour faciliter la vie du programmeur (quand l'assembleur n'était pas encore très utilisé et que les données étaient manuellement codées en binaire) le système "BCD" (Binary Coded Decimal) a été créé.

A la place de coder un nombre en binaire directement : 

$$
14\_{10} = 1110\_2
$$

On va le codé par équivalent décimal :

$$
14\_{10} = 0001 0100\_2
$$

Ce système, bien que plus facile pour le programmeur est plus complexe pour le processeur, donc avec l'arrivée de l'assembleur et autres, ce système est vite abandonné.

<!-- Addition en BCD -->

### Représentation de nombres naturels
L'espace mémoire pour la représentation d'un nombre est toujours un nombre entier d'octet, la taille dépends de la valeur représentée :

| Bits | Nom FR | Nom EN |
| ---- | --- | --- |
| 8 | Octet | Byte |
| 16 | Entier court | Short integer |
| 32 | Entier | Integer |
| 64 | Entier long | Long integer |

Si un nombre représenté laisse des emplacements libres sur la gauche, on les remplace par des 0 pour entrer dans l'emplacement mémoire prévu.

Par exemple pour représenter 5 dans un emplacement mémoire de 8 bits. On le transforme en binaire et on obtient 101 mais pour le faire entrer dans un octet cela va être 00000101.

* Pour connaitre le nombre de configurations possible de $n$ bits :

$$
2^n
$$

* Pour connaitre le nombre maximal pouvant être représenté en $n$ bits (on diminue le nombre de configuration de 1, car il y a le 0)

$$
2^n - 1
$$

* Pour savoir combien de bits il faut avoir pour représenter des nombres de 0 à $x$ :

$$
log_2(x+1)
$$

* Pour savoir la caractéristique binaire d'un multiple d'un nombre $n$ divisible par 2, il terminera par $log_2(n)$ zéros.
* Comment savoir comment évolue un nombre $n$ qui est multiplié ou divisé par un multiple de 2. Il suffit de décaler les nombres vers la droite ou vers la gauche de $log_2(n)$ 
* Comment faire une division avec reste d'un nombre $n$ multiple de 2. Il faut regarder les $log_2(n)$ bits de droite (premiers), ces bits représentent le Reste, tandis que les autres bits de droites seront le Quotient.


### Représentation des entiers relatifs
#### La convention signe + valeur
Pour représenter des chiffres négatifs aussi bien que positif on peut réserver un bit au début de l'espace mémoire pour le signe. Si ce bit est à 1, le chiffre est négatif, sinon, il est positif.

L'un des problèmes est que les opérations arithmétiques ne sont pas facile et qu'il existe la valeur +0 et -0 qui n'ont pas de sens

* Pour savoir le nombre minimal et maximal de nombres pouvant être représenté en $n$ bits. On va de $-2^{n-1}$ à $+2^{n-1}$. On soustrait 1 car il y a maintenant un bit utilisé pour représenter le signe.
* Pour savoir le nombre maximal de chiffres pouvant être représentés. 

$$
2^{n-1} * 2
$$ 


### Complément à 1
Pour faciliter les opérations arithmétiques nous pouvons utiliser une autre méthode. Celle d'inverser tous les bits.

Donc pour représenter $74_{10}$ sur 8 bits en complément à 1, on obtient `01001010` et pour changer le signe, on inverse tous les bits ce qui donne donc `10110101`. Le bit de signe est donc toujours en action.

### Complément à 2
