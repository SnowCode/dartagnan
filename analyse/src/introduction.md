## Qu'est ce que l'analyse
L'*analyse* est le procédé de raisonement qui va de la connaissance desparties à celle du tout. C'est l'opposé de la synthèse. Étude et examen sont des synonymes.

Il s'agit de déterminer les contours et le fonctionnement général de quelque chose avant de le construire. Par exemple à l'aide de dessins, plans, maquettes, etc. Cette description permet de communiquer avec les clients et les concepteurs.

En informatique il s'agit d'identifier les besoins d'un organisme ou d'un ensemble d'utilisateurs. De représenter ces besoins avec des modèles spécifiques et de faciliter la communication avec les différents acteurs (clients, analytes et concepteurs)

Les raisons d'échec d'un projet sont souvent liée à une mauvaise définition des objectifs métiers et de la formulation des besoins.

* Périmètre (ce qu'il faut faire)
* Business rules (les règles métier)
* Data modelling (les données nécessaire)
* UX design (l'expérience utilisateur)

Les règles métier sont les règles du fonctionnement de l'entreprise, comment elle fonctionne, son organisation, etc à fin de la rendre plus efficace.

L'analyste va identifier les besoins en rencontrantles clients, utilisateurs, sponsors, etc. Il va consulter les ressources existantes tel que la documentation, il pose des question et met en évidence les incohérences.

Ensuite l'analyste crée des modèles sur différents aspects du système (données, processus, fonctionalités, UI, etc) pour engager la discussion.

L'analyste va ensuite faire valider ses modèles en discuttant avec l'équipe avec l'aide d'autres documents (cahiers des charges, spécifications fonctionnelles, etc) et peut aussi participer aux aspects budgétaires (évaluation de la charge de travail)

Un modèle en informatique a pour objectif de structurer les informations et activités d'un système (données traitements, flux d'informations). Il a pour but de couvrir les points les plus importants. C'est un outil de communication et d'information sur le projet.

L'analyste estla personne qui sert de pont entre les clients et les concepteurs. Il doit pouvoir faire la "traduction" du client au programmeurs. L'analyste se doit d'être précis et rigoureux pour déceler les trous dans le discours du client. Il doit être un bon négociateur et communiquant.

Sa place dans l'organisation depends de l'entreprise, du projet et de la méthodologie utilisée.

La méthode de l'analyse est de définir les besoins, déterminer la faisabilité et la cohérence de la demande et de structurer les données. Le cours va être plus basé sur la partie identifications des besoins et modélisation de données et interface.

Le but du cours est de décortiquer et interpreter le discour du client, de maitriser les concepts, outils et vocabulaire et de pouvoir modéliser des systèmes complexes en suivant une méthode.

