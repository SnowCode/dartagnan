## Comment concevoir une UI
* Groupements par taches
* Groupements par contenus
* Définir une structure commune et cohérente. Par exemple les boutons de confirmation sont en bleu et à droite et le bouton pour annuler est à gauche en gris ou rouge. Ce sont des conventions qui doivent se retrouver sur toutes les pages du site, et de préférence commun sur internet en général.
* Material design
    * Types de navigation (verticale, horizontale, etc)
* Valider les interfaces par rapport aux Use Cases
* UX: User Experience

## MCD (Modélisation Conceptuelle des Données)
Comme vu en base de données pour faire un MCD il faut faire l'inventaire des différentes données et ensuite les groupes et établire des liens entre elles.

Les liens dans le schéma peuvent aussi mettre en évidence des contraintes tel que 1-1 (minimum 1 lien, maximum 1 lien), mettre "N" pour un nombre indéfinis de liens. Ce nombre signifie le nombre de fois que l'utilisateur peut être dans la table à laquelle est liée. Par exemple "Client (0-N) → (1-1) Véhicule" signifie que le client peut être lié 0 à N fois à la table véhicule, tandis qu'un véhicule doit toujours être lié une seule fois à la table client.

* Entité est un exemple de donnée (par exemple, client)
* Propriété
* Occurences
* Association
* Cardinalité (par exemple 1-1)
* Role
* Identifiant
