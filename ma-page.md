# Ceci est ma premiere page html
Bonjour, mon nom est Julien Lejoly et je suis en BAC 1 d'informatique de gestion. Cet exercice est realisé dans le cadre du cours d' "Initiation au développement web".

## Des paragraphes, des liens et des images
Voici un paragraphe qui contient [un lien vers le site HELMo](https://helmo.be).

Voici un paragraphe, c'est-à-dire un bloc de texte qui va être espacé des éléments précédents et suivants. Le passage à la ligne est automatique dans un paragraphe: toutefois, je peux utiliser la balise \<br> pour forcer le passage à la ligne.

---

Voici une image de mon ordi quand il marchait :
![image de mon ordi quand il marchait](rice.png)

---
