
## Ricing
Alors, non. Windows est vraiment impossible à Rice sans risquer de se prendre beaucoup de virus ou d'y perdre la tête.
Par contre il y a quelques choses à faire:

1. Supprimer les machins inutiles de la barre de tache en faisant clic droit et en sélectionnant "Désactivé" pour les choses correspondantes
2. Supprimer les ~~pubs~~ recommendations dans le menu démarrer en faisant clic droit sur les sections puis "Détacher"

## Raccourcis de clavier Windows
Clairement pas au niveau de configuration ou de détail que bspwm mais quelques trucs sont possible quand même:

* ALT+TAB pour changer de fenêtre rapidement
* WIN+[1-9] pour ouvrir les logicielsdans la barre de tache rapidement
* WIN pour rechercher un programme
* WIN+M pour tout minimiser
* WIN+SHIFT+M pour tout maximiser
* WIN+flèches pour bouger les fenêtres
* FN+ALT+F4 ou parfois CTRL+W pour fermer une fenêtre
* WIN+CTRL+flèche pour changer de bureau (workspace) (nécessite qu'il soit d'abord créé dans WIN+TAB) par contre il n'y a pas de raccourcis pour bouger le fenêtres

## Outils pratiques 
* SSHFS-win et WINFSP pour monter un répertoire d'un serveur dans le gestionaire de fichier (j'utilise ça pour utiliser le serveur dartagnan)
* Zettlr pour modifier du markdown (j'utilise Zettlr pour faire mes notes)
* Powershell pour un machin un peu plus potable pour faire des commandes
* VSCodium pour éditer du code (avec intégration Git)
* Python pour faire des scripts (j'utilise python pour générer les pages du site dartagnan)
* Firefox pour protéger un peu sa vie privée
    * Tridactyl pour aller sur internet via le clavier via les même raccourcis que VIM
    * uBlock Origin pour le meilleur adblock de la planète
    * Dark reader pour le plaisir des yeux