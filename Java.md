# Java
> **Attention!** ceci sont des notes que je fais au fur et à mesure, je ne suis pas tout à fait sûr de ce que je fais. Pour être sûr référez vous aux liens ci dessous:

## Ressources intéressantes en dehors du cours
> *Ces liens ont été proposé par moi ou d'autres étudiants sur Discord*

* [Fireship: Java in 100 Seconds](https://www.youtube.com/watch?v=l9AzO1FMgM8), est une introduction *très* rapide de Java en Anglais qui explique un peu le but et les avantages de Java ainsi que décortiquer un peu le programme Hello World et expliquer comment compiler un programme.
* [OpenClassRoom: Apprenez à utiliser la ligne de commande dans un terminal](https://openclassrooms.com/fr/courses/6173491-apprenez-a-utiliser-la-ligne-de-commande-dans-un-terminal), est une explication de l'utilisation du terminal (je n'ai pas suivi ce cours du tout donc je ne sais pas ce qu'il vaut)
* [OpenClassRoom: Installez votre environnement de développement avec Eclipse](https://openclassrooms.com/fr/courses/6106191-installez-votre-environnement-de-developpement-java-avec-eclipse), est une explication de comment installer les différents logiciels pour programmer en Java ainsi qu'une explication brève de comment Java fonctionne 
* [OpenClassRoom: Apprenez à programmer en Java](https://openclassrooms.com/fr/courses/6173501-apprenez-a-programmer-en-java), explique les différentes partie de la programmation en Java (variables, classes, méthodes, etc). Les cours d'Open Classroom sont vraiment bien fait et ont aussi des vidéos et des quizz pour tester ses connaissances.
* [Sololearn](https://www.sololearn.com/) est une platforme pour apprendre des languages de programmation par petit bout, dont Java.

![simple résumé des bases en java](ocr-java-1.png)

## Installation des logiciels nécessaires
### Installation de OpenJDK 18
* Lien de télachargement Windows [ici](https://download.java.net/java/GA/jdk18.0.2.1/db379da656dc47308e138f21b33976fa/1/GPL/openjdk-18.0.2.1_windows-x64_bin.zip)
* Lien de téléchargement macOS [ici](https://download.java.net/java/GA/jdk18.0.2.1/db379da656dc47308e138f21b33976fa/1/GPL/openjdk-18.0.2.1_macos-x64_bin.tar.gz)
* Sur Debian ou Ubuntu (ou autre distributions utilisant APT):

```bash
sudo apt install openjdk-18-jdk
```

* Sur Arch (*pacman*)

```bash
sudo pacman -S jdk-openjdk
```

* Sur Windows (avec *chocolatey*)

```batch
choco install openjdk --version=18.0.2.1
```

* Sur macOS (avec *brew*)

```bash
brew install openjdk@18.0.2.1
```

Enfin pour tester si java a bien été installé on peut lançer la commande:

```bash
java -version
```

### Installation de Eclipse
* Sur Arch (un paquet AUR `eclipse-java` est disponible)

```bash
yay eclipse-java
```

* Sur macOS (avec `brew`)

```bash
brew install --cask eclipse-java
```

* Sur Windows (avec `chocolatey`)

```powershell
choco install eclipse-java-oxygen
```

* Autre distribution Linux (nécessite d'avoir `wget` installé). L'alernative au commandes, c'est d'aller [sur le site](https://eclipseide.org/), de télécharger Eclipse, d'extraire le `.tar.gz` et de lançer l'installateur. Ou un tutoriel imagé est disponible [ici](https://linuxhint.com/install_eclipse_ide_debian_10/)

```bash
wget https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2022-09/R/eclipse-inst-jre-linux64.tar.gz
tar xvf eclipse-inst-jre-linux64.tar.gz
cd eclipse-installer/
./eclipse-inst
```

## Hello World
* Une fois `openjdk-18` installé, on peut créer un dossier "Java" et y mettre un autre dossier "prb"

```bash
mkdir -p Java/prb
cd Java/
```

* Ensuite on peut y ajouter un fichier `Bonjour.java`. À noter que le fichier doit être noté de cette manière. 
    * Le dossier doit être du même nom que le `package` du projet (qui contient les classes)
    * Le fichier doit être noté de la même manière que la classe `public` qui est dans le fichier (qui est contenu dans le `package`)

```bash
nano prb/Bonjour.java
```

* Ajouter le code suivant:

```java
// Le package contient les classes, cette ligne indique que le fichier fait partie du package "prb"
// Le package doit être du même nom que le dossier dans lequel le fichier est
package prb;

// La classe doit être du même nom que le fichier, dans ce cas ci la classe "Bonjour" doit être dans un dossier appellé "Bonjour.java"
// Tout le code en java est écrit dans des classes. La mention "public" signifie que cette classe peut être réutilisée par d'autres classes ailleurs
public class Bonjour {
    // static veut dire que ce n'est pas un "objet" de la classe mais est associée. Il ne faut pas créer une instance de la classe pour appeller cette fonction/méthode
    // void signifie qu'il n'y a pas de valeur "return" à la méthode. 
    // la méthode main est la fonction recherchée par java quand la classe est lançée. C'est ici qu'est le programme principal
	public static void main(String[] args) {
        // System est une classe par défault de Java
	    // println est une méthode (ou fonction) du bloc "out" de la classe "System"
		System.out.println("Bonjour !");
	}
}
```

* Faire une première compilation du code, pour créer un fichier `Bonjour.class`

```bash
javac prb/Bonjour.class
```

* Faire une deuxième compilation pour rendre le code exécutable

```bash
java prb.Bonjour
```
