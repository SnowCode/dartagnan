# Comment se connecter au serveur Dartagnan sans Filezilla
Je trouve que Filezilla est assez peu pratique à utiliser alors j'utilise un autre logiciel appellé "sshfs" qui permet de monter un répertoire distant (comme celui de dartagnan par exemple) comme si c'était une clé USB sur l'ordinateur.

Alors voici quelques tutoriels sur comment l'installer et l'utiliser

## Sur Windows
<video controls src="sshfs-win.mp4" ></video>

1. Installer [WinFSP](https://github.com/winfsp/winfsp/releases/tag/v1.11) et [sshfs-win](https://github.com/winfsp/sshfs-win/releases/tag/v3.7.21011)
2. Aller dans l'explorateur de fichier et cliquer droit sur `Mon PC` puis choisir `Connecter un lecteur réseau` 
3. Entrer `\\sshfs\q123456@dartagnan.cg.helmo.be` (remplacer *q123456* par le numéro de matricule) puis entrer le mot de passe du compte HELMo.

## Sur Linux ou macOS
<video controls src="sshfs-linux.mkv" ></video>

1. Installer sshfs
    * Linux: Installer le paquet `sshfs`
    * macOS: Installer OSX fuse et SSHFS depuis [ce site](https://osxfuse.github.io/)
2. Créer un dossier dartagnan : `mkdir dartagnan`
3. Monter le serveur dans le dossier `sshfs dartagnan q123456@dartagnan.cg.helmo.be:/home/q123456` en remplacant toujours `q123456` par le matricule approprié.
