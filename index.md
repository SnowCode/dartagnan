# Snow's site
Coucou, moi c'est [Julien](Julien.html). Voilà. Ce site contient principalement des tutoriels et des notes de cours. 

## Raccourcis
* [Helmo Learn](https://learn-technique.helmo.be)
* [Serveur Discord *Helmo - Informatique/Sécurité*](https://discord.gg/3vWtK5QMcw)
* [Mails Helmo](https://outlook.office.com/)
* [Helmo Connect](https://connect2.helmo.be/TableauDeBord/Etudiant/Index)

## Perso
* [todo](todo.html) pour mes trucs à faire

## Notes de cours / synthèses
* [Java](java/book) pour les guide d'installation d'OpenJDK 18, Eclipse ainsi que quelques résumés.
* Notes du cours d'[Architecture des ordinateurs](archi/book)
* Notes du cours de [développement web théorique](devweb.html)
* Notes du cours d'[Analyse](analyse.html)
* Notes du cours de [base de données](bd.html)
* [Math](math/book)

## Tutos
* [HelmoTuto](HelmoTuto.html) pour des tutoriels relatifs à l'infrastructure de l'école (VPN, Wifi, serveur, etc)
* [VPN](vpn.html) pour créer son propre VPN (Wireguard) avec un Raspberry PI
* [Statique](statique.html) tutoriel pour créer un site statique comme celui ci facilement
* [Barrier](barrier.html) pour apprendre à configurer Barrier pour utiliser le clavier, souris et presse-papier d'un ordi pour en controler d'autres.
* [irc](irc.html) pour créer un réseau IRC (sécurisé, anonyme et avec un client web)
* [bspwm](bspwm.html) pour configurer bspwm en quelques minutes
* [sshfs](sshfs.html) pour remplacer Filezilla par quelque chose de beaucoup plus facile à utiliser.
