## Acquisition des données de l'utilisateur (lire dans la console, stdin)
Créer un dossier `io` dans `premier-java` et y placer le fichier `Console.java` donné dans la page du cours. Il est possible qu'il soit nécessaire de changer le package du fichier vers `io` si ce n'est pas déjà le cas. Ensuite dans notre Hello.java (ou autre). 

```java
// io (package) .Console (classe) .lireInt() (méthode)
int variableInput = io.Console.lireInt();
System.out.println(variableInput);
```

La classe Console contient plusieurs méthodes: `lireString`, `lireChar`, `lireInt`, `lireLong`, `lireFloat`, `lireDouble` pour récupérer des informations depuis une saisie de l'utilisateur dans le terminal.

Cette classe Console est une simplification du code Java de base pour aller plus vite.

Pour savoir comment la saisie fonctionne dans le java de base, il suffit de lire le code de Console.java. 

### En savoir plus
* Voir le fichier Console.java

