## Variables (types, constantes, cast, String)
### Les types primitifs
```java
/* Code à intégrer dans la fonction main d'une classe */

// On peut déclarer une variable avant de l'initier
int entier;
entier = 42;

// Ou on peut faire les deux en même temps
boolean test = true;
char lettre = 'A'; // Attention, le ' est nécessaire et ne peut pas être remplacé par "
byte octet = 127
double nombreAVirgule = 5.2;

// On peut aussi créer une constante, sa valeur ne pourra pas changer
final int reponseALaVie = 42;
```

Il y a 8 types primitifs en Java: 

| Type | Taille en bits | Domaine de valeurs | Information supplémentaire |
| ---- | -------------- | ------------------ | -------------------------- |
| `boolean` | ? | 0 (false), 1 (true) | La taille dépends de la JVM |
| `char` | 16 | N'importe quel caractère unicode | N/A |
| `byte` | 8 | $ -128 $ → $ +127 $ | N/A |
| `short` | 16 | $ -2^{15} $ → $ 2^15 - 1 $ | N/A |
| `int` | 32 | $ -2^{31} $ → $ 2^{31} - 1 $ | N/A |
| `long` | 64 | $ -2^{63} $ → $ 2^{63} -1 $ | N/A |
| `float` | 16 | $ -3.4 * 10^{38} $ → $ 3.4 * 10^{38} $ | Nombre à virgule (virgule flotante). La précision est de $ 1.4 * 10^{-45} $ |
| `double` | 64 | $ -1.7 * 10^{308} $ → $ 1.7 * 10^{308} $ | Comme `float` mais avec une précision de $ 4.9 * 10^{-324} $ |

### Convertir entre les types avec cast

```java
// Conversion de float en int avec cast
double x = 42.5;
int y = (int)x; // 42
int z = (int)42.5; // 42
int a = (int)(42.0 + 0.5); // 42
```

On peut aussi transformer une valeur en une autre en utilisant un *cast*. Comme vu ci dessus en précisant le type cible en (). 

A savoir qu'ici, dans le cas d'une conversion d'un nombre en virgule flotante en entier, on perds les informations des décimales. Cast va toujours arrondir vers le bas dans une conversion comme celle ci. Pour avoir plus de controle sur l'arrondis, on peut utiliser la classe java Math (voir plus tard).

### Introduction aux Strings (chaine de caractères)

```java
String maChaine = "Hello, World!";
System.out.println(maChaine);

// Convertir un string en un int (cast est impossible car String est une classe)
String nombreStr = "42"; // "42"
int nombreInt = Integer.parseInt(nombreStr); // 42
```

String n'est pas un type primitif, mais bien une classe (ce qui est pourquoi la première lettre est en majuscule) qui corresponds à une chaine de caractère, dans un chapitre suivant nous allons voir quelques méthodes qui peuvent être appliquer aux objets de la classe "String". 

N'étant pas un type primitif nous ne pouvons pas utiliser cast dessus mais on peut utiliser d'autres méthodes d'autres classes comme `Integer.parseInt()`, il existe aussi d'autres méthodes du même genre comme `Double.parseDouble()`. 

### En savoir plus
* [Wikiversity FR - Java: Variables et types](https://fr.wikiversity.org/wiki/Java/Variables_et_types)
* [Oracle Docs - String](https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/lang/String.html)
* [Oracle Docs - Integer.parseInt](https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/lang/Integer.html#parseInt(java.lang.String))

