## Hello World
Pour le cours de Java on va utiliser OpenJDK 18. On peut l'installer depuis [le site de java OpenJDK](https://jdk.java.net/18/). JDK veut dire "Java Development Kit" et openJDK est une implémentation de cela en open source. On peut aussi installer un éditeur de code (exemple: Notepad++, Atom, Visual Codium).

Une fois cela fait, on peut créer un dossier pour notre projet "premier-projet" dans lequel on va mettre un autre dossier "hello". Dans ce dossier on va créer un fichier `Hello.java`. Attention que les noms, doivent être exacts à l'exception du premier dossier "premier-projet".

Dans le fichier Hello.java, on peut ajouter le code suivant

```java
// Le package contient les classes, cette ligne indique que le fichier fait partie du package "prb"
// Le package doit être du même nom que le dossier dans lequel le fichier est
package hello;

// La classe doit être du même nom que le fichier, dans ce cas ci la classe "Bonjour" doit être dans un dossier appellé "Bonjour.java"
// Tout le code en java est écrit dans des classes. La mention "public" signifie que cette classe peut être réutilisée par d'autres classes ailleurs
public class Hello {
    // static veut dire que ce n'est pas un "objet" de la classe mais est associée. Il ne faut pas créer une instance de la classe pour appeller cette fonction/méthode
    // void signifie qu'il n'y a pas de valeur "return" à la méthode. 
    // la méthode main est la fonction recherchée par java quand la classe est lançée. C'est ici qu'est le programme principal
    public static void main(String[] args) {
        // System est une classe par défault de Java
        // println est une méthode (ou fonction) du bloc "out" de la classe "System"
        System.out.println("Bonjour !");
    }
}
```

On peut enfin lançer ce programme en ouvrant l'invite de commande dans le dossier `premier-java` et en lançant 

```batch
javac hello\Hello.java
java hello.Hello
```

La commande javac va compiler un fichier .class. Java est un language hybride qui compile une moitié de code (qui est le bytecode) prévu pour un processeur virtuel qui sera émulé par JVM (Java Virtual Machine) pour pouvoir fonctionner sur toutes les platformes. Le slogan de Java "Write once, run everywhere" viens de là.

