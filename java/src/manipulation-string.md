## Manipulation de Strings

```java
String inputString = "ceci est mon string, 32";
String[] tab = inputString.split(", ");
String str = tab[0].toUpperCase();
int number = Integer.parseInt(tab[1]);
System.out.printf("%s =  %d %n", str, number);

// Tester des strings
boolean finiParTest = inputString.endsWith("test"); // false
boolean commenceParCeci = inputString.startsWith("ceci"); // true

// Avoir la longueur d'un string
System.out.println(inputString.length()); // 23
```

String étant une classe, elle contient plusieurs méthodes, en voici 2 qui soit très utiles :

* `.split("regex")` pour diviser un String en un tableau en utilisant un autre String comme délimiteur de la colonne.
* `.toUpperCase()` et `.toLowerCase()` pour convertir un String en majuscule ou minuscule
* `.endsWith()` et `.startsWith()` pour tester si un String termine ou commence par un certain substring.
* `.length()` pour obtenir la longeur de la chaine de caractères.

### En savoir plus
* [Oracle Docs - String](https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/lang/String.html)

