## Math (arrondis, puissances, racines, nombre spéciaux, etc)

```java
// Génération de nombres aléatoires
double nombreAleatoire = Math.random();

// Puissances et racines
double puissance2 = Math.pow(5.0, 2.0); // 5^2
double racineCarree = Math.sqrt(puissance2);

// Arrondir un nombre 
double nombre = 5.67;
System.out.println(Math.ceil(nombre));  // 6.0
System.out.println(Math.floor(nombre)); // 5.0
System.out.println(Math.round(nombre)); // 6
System.out.println(Math.rint(nombre));  // 6.0 
```

Pour générer une valeur aléatoire entre 0 et 1 on peut utiliser la méthode `Math.random()`. 

Pour faire une puissance de 2 on peut utiliser `Math.pow(double a)` et pour faire une racine `Math.sqrt(double a)`. Toutes ces fonctions renvoie des valeurs de type `double`

Pour faire des arrondis on peut utiliser différentes fonctions :

* `Math.ceil(nombre)` pour arrondir un nombre vers le haut (retourne un double)
* `Math.floor(nombre)` pour arrondir un nombre vers le bas (retourne un double)
* `Math.round(nombre)` pour arrondir un nombre en fonction de sa première décimale (retourne un int)
* `Math.rint(nombre)` pour arrondir un nombre en fonction de sa première décimale (retourne un double)

### Pour en savoir plus
* [Oracle Docs - Math](https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/lang/Math.html)
