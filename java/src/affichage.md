## Affichage formatté (print, println, printf)

```java
System.out.print("Quel est votre nom ? ");
String nom = io.Console.lireString();
System.out.println("Hello World");
System.out.printf("Hello %s", nom);

// Intégrer un nombre décimal
int age = 17;
System.out.printf("Hello %s, you are %d", name, age);

// Intégrer un nombre à virgule flotante
double temperature = 16.5;
System.out.printf("The temperature today is %.2f\n", temperature); // The temperature today is 16,50
```

Il existe 3 fontions principales de PrintStream:

* `print` qui va afficher une valeur sans retour à la ligne
* `println` qui va afficher une valeur avec un retour à la ligne automatique (ce qui est comme ajouter \n à la fin de la valeur dans print)
* `printf` qui va permettre de faire un "template"` pour afficher des valeurs. 

Printf prends une grande variété de conversions, en voici quelques basiques :

* `%c` pour afficher un `char`
* `%s` pour afficher un `String`
* `%d` pour afficher un nombre décimal (`byte`, `short`, `int`, `long`)
* `%f` pour afficher un nombre à virgule flotante (`float`, `double`)
* `%%` pour afficher un '%'
* `%n` pour afficher un retour à la ligne

La syntaxe est `%[longueur][conversion]` par exemple, `%.5s` va afficher un `String` d'une longueur de 5 caractères. Si le `String` est moins long, `printf` va remplacer l'espace manquant par des espaces, si elle est trop courte, la valeur va être coupée. 

### En savoir plus
* [Oracle Docs - Formatter](https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/util/Formatter.html) pour plus d'information sur les conversions avec printf
* [Oracle Docs - PrintStream](https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/io/PrintStream.html) pour voir les différentes fonction de System.out.
