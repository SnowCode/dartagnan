## Opérations sur des variables (calcul, logique, binaire)

```java
int a = 12;
int b = 20;
int c = 42;

// Opérateurs de calcul
int x = (a + b + 2) / c; // Le résultat sera tronqué car x est un int et non pas un double, donc les décimales ne seront pas prise en compte
int y = c % 3; // Effectue de reste d'une division euclidienne

// Opérateurs lors de l'assignation d'une variable
a = a + 5 // Ajoute 5 à la variable a
a += 5; // Ajoute 5 à la variable a
b++; // Ajoute 1 à la variable b

// Opérateurs logiques / binaires
boolean a = true && false; // false
boolean b = true || false; // true
boolean c = !false; // true

// Ou exclusif, 1 et 1 donne 0 mais 1 et 0 donne 1
boolean d = true ^ true; // false
boolean e = true ^ false; // true
```

Il existe 5 opérateurs de base en Java:

* `+` : addition
* `-` : soustraction
* `* `: multiplication
* `/` : division
* `%` : modulo (reste d'une division euclidienne, exemple $ 13 = 5 * 2 + 3 $ donc $ 13 \mod 5 = 3 $

Il existe ensuite plusieurs opérateurs logiques et binaires, mais on va se concentrer sur les principaux uniquement

* `&&` : ET (true + true = true)
* `||` : OU inclusif (true + false = true) et (true + true = true)
* `^` : OU exclusif (true + false = true) et (true + true = false) 
* `!` : NEGATION (true = false) et (false = true)

On peut ensuite effectuer des modifications lors de l'affectation de la variable directement pour gagner du temps en ajoutant l'opérateur devant le signe `=`

On peut aussi ajouter 1 ou diminuer un rapidement en écrivant `x++` ou `x--` par exemple. 

### En savoir plus
* [Wikiversity FR - Java: Opérations](https://fr.wikiversity.org/wiki/Java/Op%C3%A9rations)

