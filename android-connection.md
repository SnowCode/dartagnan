# Connecter un téléphone (Android) à un ordi
## Utiliser scrcpy pour voir et contrôler un téléphone sur PC
* Installer *scrcpy*

```bash
sudo apt install scrcpy
```

* Aller dans les paramètres du téléphone dans *About phone* → spammer *Build number* pour déverouiller les *Paramètres développeurs* et activer *USB Debugging*
* Lancer `scrcpy`, plusieurs modes sont disponibles

```bash
# Défault
scrcpy 

# Pour garder le téléphone allumé
scrcpy -w

# Pour garder le téléphone allumé et l'écran éteint
scrcpy -Sw
```

Pour plus d'information voir [ici](https://github.com/Genymobile/scrcpy).

## Utiliser la connection d'un ordi sur téléphone
A faire

## Utiliser le clavier d'un ordi sur téléphone
A faire
