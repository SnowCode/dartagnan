# Trucs à faire
> Ceci est une page perso, si vous n'êtes pas moi, cliquez [ici](https://www.youtube.com/watch?v=p7YXXieghto).

## A faire (non urgent)
* Lire les dias de prb théorique
* Faire la synthèse de math et avancer dans le cours
* Ajouter des images et terminer la synthèse sur les bases de données
<!-- Autohéberger caligraphies + faire capsule gemini --> 

## A faire ou en cours (plus urgent)
* Reformuler la synthèse d'analyse
    * Refaire le devoir Vélib et faire le tuto en fonctoin
    * Trouver un lien vers un tuto pour les UC
* Architecture des ordinateurs
    * Résumer la structure des ordinateurs
        * Définition 
        * Gammes
        * Cloud
            * Services
        * Interne (bus)
        * Architecture en couches
        * CISC vs RISC
    * Résumer la conversion entre bases
    * Gestion des nombres négatifs en binaire
* Faire la synthèse de BDD 
* Ajouter un lien pour la spécificatoin devweb
* Se préparer pour le test pratique de prb

## Terminé
* Faire la synthèse d'analyse
* Voir pour le cours de programmation de base
* Voir pour les cours de RIEP
* Faire la synthèse d'archi
* Configurer un autre PC de remplacement
* Ajouter des images/vidéos pour le cours d'[archi](archi.html)
* Installer les machins (IDE, OpenJDK 18, Eclipse, )
* Écrire le début du résumé de [Java](Java.html)
* Résumer le premier cours d'archi
* Mettre des couleurs dans les blocs de code de ce site + math stuff
* Lire la FAQ du cours d'anglais & aller voir le syllabus
* Trouver une alternative sympa à `dwm`
* Écrire la documentation du script de ce site
* Refaire le CSS de ce site
* S'incrire au cours de programmation de base (b1info)
