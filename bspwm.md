# Ma configuration très minimale de bspwm
![screenshot de ma config bspwm](rice.png)

J'ai utilisé pendant un long moment *dwm*, mais dès qu'il y a un changement dans le code source, ça devient en enfer à reconfigurer. Donc j'ai commencé à chercher autre chose.

Ici je suis tombé sur *bspwm* qui est extrèmement bien et très simple à configurer.

Contrairement à *dwm*, qui se configure en *C*; *bspwm* se configure en shell avec de simple commandes.

Donc je vais montrer ma config de quelque logiciels:

* *bspwm*, le gestionaire de fenêtres en lui même
* *sxhkd*, pour tous les raccourcis de claviers
* *alacritty* (ou autre terminal), pour le terminal lol
* *rofi* (ou *dmenu*), pour le lancement des applications
* *pywal*, pour mettre un fond d'écran et changer le thème du terminal en fonction
* *polybar*, pour avoir une barre avec quelques informations
* *picom*, pour avoir des animations simpa (un fork)

## Le tuto
* Installer les machins

```bash
sudo pacman -S bspwm sxhkd alacritty rofi polybar picom-<fork>
pip install pywal
```

* Copier les fichiers de configuration

```bash
mkdir ~/.config/bspwm ~/.config/sxhkd ~/.config/polybar
cp /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm
chmod +x ~/.config/bspwm
cp /usr/share/doc/bspwm/examples/sxkhdrc ~/.config/sxhkd
cp /etc/polybar/config.ini ~/.config/polybar
```

* Modifier les fichiers de configuration (pour bspwm, changer les logiciels et ajouter polybar au démarrage) (pour sxhkd, aller dedans pour voir les raccourcis, changer les logiciels, etc)
    * Pour *bspwm*, ajouter `polybar &` et `wal -i ~/wallpapers/ --saturate 1 &` au démarrage, suprimmer ce qui n'est pas nécessaire, changer les valeurs 
    * Pour *sxhkd*, changer les logiciels par default, changer les raccourcis (en partiulier pour supporter AZERTY)
    * Pour *polybar*, supprimer les modules inutiles, changer les couleurs, voir [ici](https://github.com/polybar/polybar/wiki) pour ajouter ou reconfigurer des modules
    * Pour *alacritty*, ajouter les fonts à utiliser et l'opacité (voir plus bas)

```bash
nano ~/.config/bspwm/bspwmrc
nano ~/.config/sxhkd/sxhkdrc
nano ~/.config/polybar/config.ini
nano ~/.config/alacritty/alacritty.yml
```

* Changer le fichier `.xinitrc` pour lancer 

```bash
echo -e '#!/bin/sh\nexec dbus-launch bspwm' >> ~/.xinitrc
chmod +x ~/.xinitrc
```

* Ajouter tous les fonds d'écran dans le dossier et charger les couleurs dans le *.bashrc*


```bash
echo "cat ~/.cache/wal/sequences" >> ~/.bashrc
mkdir ~/wallpapers
cd wallpapers
# ICI
```

* Redémarrer l'ordi

```bash
reboot
```

## Configuration d'alacritty

```yml
font:
  normal:
    family: Agave
    style:
  bold:
    family: Agave
    style: Bold
  italic:
    family: Agave
    style: Italic
  bold_italic:
    family: Agave
    style: Bold Italic
  size: 15
  use_thin_strokes: true

background_opacity: 0.95
```
