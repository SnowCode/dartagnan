<style>
@charset "UTF-8";

@font-face {
  font-family: "et-book";
  src: url("https://edwardtufte.github.io/et-book/et-book/et-book-roman-line-figures/et-book-roman-line-figures.eot");
  src: url("https://edwardtufte.github.io/et-book/et-book/et-book-roman-line-figures/et-book-roman-line-figures.eot?#iefix") format("embedded-opentype"), url("https://edwardtufte.github.io/et-book/et-book/et-book-roman-line-figures/et-book-roman-line-figures.woff") format("woff"), url("https://edwardtufte.github.io/et-book/et-book/et-book-roman-line-figures/et-book-roman-line-figures.ttf") format("truetype"), url("https://edwardtufte.github.io/et-book/et-book/et-book-roman-line-figures/et-book-roman-line-figures.svg#etbookromanosf") format("svg");
  font-weight: normal;
  font-style: normal
}

@font-face {
  font-family: "et-book";
  src: url("https://edwardtufte.github.io/et-book/et-book/et-book-display-italic-old-style-figures/et-book-display-italic-old-style-figures.eot");
  src: url("https://edwardtufte.github.io/et-book/et-book/et-book-display-italic-old-style-figures/et-book-display-italic-old-style-figures.eot?#iefix") format("embedded-opentype"), url("https://edwardtufte.github.io/et-book/et-book/et-book-display-italic-old-style-figures/et-book-display-italic-old-style-figures.woff") format("woff"), url("https://edwardtufte.github.io/et-book/et-book/et-book-display-italic-old-style-figures/et-book-display-italic-old-style-figures.ttf") format("truetype"), url("https://edwardtufte.github.io/et-book/et-book/et-book-display-italic-old-style-figures/et-book-display-italic-old-style-figures.svg#etbookromanosf") format("svg");
  font-weight: normal;
  font-style: italic
}

@font-face {
  font-family: "et-book";
  src: url("https://edwardtufte.github.io/et-book/et-book/et-book-bold-line-figures/et-book-bold-line-figures.eot");
  src: url("https://edwardtufte.github.io/et-book/et-book/et-book-bold-line-figures/et-book-bold-line-figures.eot?#iefix") format("embedded-opentype"), url("https://edwardtufte.github.io/et-book/et-book/et-book-bold-line-figures/et-book-bold-line-figures.woff") format("woff"), url("https://edwardtufte.github.io/et-book/et-book/et-book-bold-line-figures/et-book-bold-line-figures.ttf") format("truetype"), url("https://edwardtufte.github.io/et-book/et-book/et-book-bold-line-figures/et-book-bold-line-figures.svg#etbookromanosf") format("svg");
  font-weight: bold;
  font-style: normal
}
</style>

## Diagramme « Use case »
![UC](./UC.png)

## Spécification textuelle
### Ajouter livre
* **Déclencheur** : La cheffe-bibliothécaire désire ajouter un livre reçu dans l'inventaire
* **Description** : La cheffe-bibliothécaire clique sur "ajouter un livre" et encode ou scanne le code barre. Le livre est automatiquement répertorié dans l'inventaire à l'aide de son ISBN.
* **Acteur·ice·s** : Cheffe-bibliothécaire
* **Pré-conditions**  : Le livre doit avoir un code barre
* **Post-conditions** : Le livre doit être encodé dans l'inventaire

### Supprimer livre
* **Déclencheur** : La cheffe-bibliothécaire voit qu'un livre est trop vieux ou trop abîmé et souhaite le retirer de l'inventaire
* **Description** : La cheffe-bibliothécaire cherche le livre dans l'inventaire ou le trouver en le scannant et clique sur "Supprimer de l'inventaire"
* **Acteur·ice·s** : Cheffe-bibliothécaire
* **Pré-conditions**  : Le livre doit exister dans l'inventaire
* **Post-conditions** : Le livre doit être retiré de l'inventaire

### Scanner livre
* **Déclencheur** : La bibliothécaire souhaite scanner le code-barre pour trouver un livre dans l'inventaire (pour emprunter, etc)
* **Description** : La biliothécaire scanne le livre et la page sur le livre apparait à l'écran
* **Acteur·ice·s** : Bibliothécaire
* **Pré-conditions**  : Le livre doit déjà exister dans l'inventaire
* **Post-conditions** : La page est ouverte et des actions sont possibles

### Emprunter livre
* **Déclencheur** : Un emprunteur souhaite emprunter un livre
* **Description** : La bibliothécaire scanne le livre ou le cherche et clique sur "Emprunter" et encode les coordonnées de contact de l'emprunteur ainsi que la date (autocomplètée automatiquement)
* **Acteur·ice·s** : Bibliothécaire
* **Pré-conditions**  : Le livre ne doit pas déjà être emprunté, et doit exister dans l'inventaire. L'emprunteur ne doit pas avoir déjà emprunté plus de 4 livres.
* **Post-conditions** : Le livre est mis comme "emprunté" et les coordonnées encodées. L'emprunteur doit rammener le livre dans les 30 jours.

### Envoyer rappel
* **Déclencheur** : La cheffe-bibliothécaire peut envoyer un rappel au bout de 30 jours à l'emprunteur si le livre n'est toujours pas revenu
* **Description** : La cheffe-bibliothécaire peut recevoir une notification quand une personne n'a pas rammené son livre à temps (dans un délais de 30 jours) ou en consultant manuellement la page du livre emprunté et peut générer en un clic une lettre recommandée
* **Acteur·ice·s** : Cheffe-bibliothécaire
* **Pré-conditions**  : Le délais de 30 jours doit être passé
* **Post-conditions** : Aucun

### Rechercher livre
* **Déclencheur** : La bibliothécaire souhaite trouver un livre selon divers critères
* **Description** : La bibliothécaire peut sélecttionner avec l'outil de recherche l'auteur, le titre, le genre ou des mot-clés et le  système retourne la liste des livres qui correspondent à la recherche.
* **Acteur·ice·s** : Bibliothécaire
* **Pré-conditions**  : Le livre doit exister dans l'inventaire ainsi que les données associées
* **Post-conditions** : Aucun

### Voir détails livre
* **Déclencheur** : La bibliothécaire souhait eavoir plus d'information sur un livre en particulier
* **Description** : La bibliothécaire cherche le livre et peut voir les détails du livre ainsi que si il est emprunté ou non
* **Acteur·ice·s** : Bibliothécaire
* **Pré-conditions**  : Le livre doit exister dans l'inventaire
* **Post-conditions** : Aucune
